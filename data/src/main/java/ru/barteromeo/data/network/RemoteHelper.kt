package ru.barteromeo.data.network

import com.google.gson.GsonBuilder
import com.itkacher.okprofiler.BuildConfig
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.barteromeo.data.UserPreferences
import ru.barteromeo.data.http.AuthTokenInterceptor
import ru.barteromeo.data.http.ConnectivityInterceptor
import ru.barteromeo.data.http.TokenAuthenticator
import javax.inject.Inject

class RemoteHelper @Inject constructor(
    private val connectivityInterceptor: ConnectivityInterceptor,
    private val authTokenInterceptor: AuthTokenInterceptor,
    private val userPreferences: UserPreferences
) {

    companion object {
        private const val BASE_URL = "http://188.130.139.161:8080"
        private const val DATE_FORMAT = "MM dd, yyyy HH:mm:ss"
    }

    fun <Api> buildApi(
        api: Class<Api>
    ): Api {
        val authenticator = TokenAuthenticator(buildAuthApi(), userPreferences)
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getRetrofitClient(authenticator))
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().setDateFormat(DATE_FORMAT).create()
                )
            )
            .build()
            .create(api)
    }

    fun buildAuthApi(): AuthApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getRetrofitClient(shouldAddAuthTokenInterceptor = false))
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().setDateFormat(DATE_FORMAT).create()
                )
            )
            .build()
            .create(AuthApi::class.java)
    }

    private fun getRetrofitClient(
        authenticator: Authenticator? = null,
        shouldAddAuthTokenInterceptor: Boolean = true
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(connectivityInterceptor)
            .addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder().also {
                    it.addHeader("Accept", "application/json")
                }.build())
            }
            .also { client ->
                authenticator?.let { client.authenticator(it) }
                val logging = HttpLoggingInterceptor()
                logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                client.addInterceptor(logging)
                if (shouldAddAuthTokenInterceptor) {
                    client.addInterceptor(authTokenInterceptor)
                }
                client.addInterceptor(OkHttpProfilerInterceptor())
            }.build()

    }
}