package ru.barteromeo.data.network

import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import ru.barteromeo.data.dto.AuthDto
import ru.barteromeo.data.dto.ConfirmEmailDto
import ru.barteromeo.data.dto.PasswordRecoverDto
import ru.barteromeo.data.requests.ConfirmEmailRequest
import ru.barteromeo.data.requests.LoginRequest
import ru.barteromeo.data.requests.PasswordRecoverConfirmRequest
import ru.barteromeo.data.requests.PasswordRecoverNewRequest
import ru.barteromeo.data.requests.PasswordRecoverRequest
import ru.barteromeo.data.requests.RegisterRequest
import ru.barteromeo.data.requests.RequestEmailConfirmRequest

interface AuthApi {
    // get refresh and access tokens
    @POST(value = "/auth/login")
    suspend fun login(@Body body: LoginRequest): AuthDto

    @POST(value = "/auth/register")
    suspend fun register(
        @Body body: RegisterRequest
    ): AuthDto

    // get access token/refresh
    @POST(value = "/auth/refresh-access-token")
    suspend fun login(@Header("Authorization") refreshToken: String?): AuthDto

    // logout
    @DELETE(value = "/auth/logout")
    suspend fun logout(@Header("Authorization") refreshToken: String)

    // password recover request
    @POST(value = "/auth/password-recovery/request")
    suspend fun passwordRecoveryRequest(@Body body: PasswordRecoverRequest)

    // password recover confirm
    @POST(value = "/auth/password-recovery/confirm")
    suspend fun passwordRecoveryConfirm(@Body body: PasswordRecoverConfirmRequest): PasswordRecoverDto

    // password recover
    @POST(value = "/auth/password-recovery")
    suspend fun passwordRecovery(@Body body: PasswordRecoverNewRequest)

    // request confirm code
    @POST(value = "/auth/email-validation/request")
    suspend fun requestEmailConfirmation(@Body body: RequestEmailConfirmRequest)

    // confirm code
    @POST(value = "/auth/email-validation/confirm")
    suspend fun confirmEmail(@Body body: ConfirmEmailRequest): ConfirmEmailDto

}