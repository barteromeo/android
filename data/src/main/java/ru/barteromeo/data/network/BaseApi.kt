package ru.barteromeo.data.network

import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import ru.barteromeo.data.dto.CategoryListDto
import ru.barteromeo.data.dto.ChatHistoryDto
import ru.barteromeo.data.dto.ChatListDto
import ru.barteromeo.data.dto.OfferDto
import ru.barteromeo.data.dto.OfferListDto
import ru.barteromeo.data.dto.UploadResponseDto
import ru.barteromeo.data.dto.UserDto
import ru.barteromeo.data.requests.CreateOfferRequest
import ru.barteromeo.data.requests.SearchRequest
import ru.barteromeo.data.requests.UserInfoRequest

@Suppress("TooManyFunctions")
interface BaseApi {

    @GET(value = "user")
    suspend fun getUserInfo(): UserDto

    @GET(value = "user/{id}")
    suspend fun getUserInfoById(@Path("id") accountId: Long): UserDto

//    @POST(value = "user")
//    suspend fun getUserInfoById(@Body body: GetUserByIdRequest): UserDto

    @POST(value = "user")
    suspend fun changeUserInfo(@Body body: UserInfoRequest)

    @GET(value = "offer/{id}")
    suspend fun resolveOfferById(
        @Path("id") id: Long
    ): OfferDto

    @POST(value = "/offer/search")
    suspend fun resolveOffers(
        @Body searchRequest: SearchRequest,
        @Query("pageSize") pageSize: Int,
        @Query("pageNumber") pageNumber: Int
    ): OfferListDto

    @GET(value = "/offer/authored")
    suspend fun resolveMyOffers(
        @Query("pageSize") pageSize: Int,
        @Query("pageNumber") pageNumber: Int
    ): OfferListDto

    @GET(value = "/wishlist")
    suspend fun resolveWishlist(
        @Query("pageSize") pageSize: Int,
        @Query("pageNumber") pageNumber: Int
    ): OfferListDto

    @POST(value = "/wishlist")
    suspend fun addToWishlist(@Body id: Long)

    @DELETE("/wishlist/{id}")
    suspend fun deleteFromWishlist(@Path("id") id: Long)

    @Multipart
    @POST(value = "/resource/upload")
    suspend fun uploadFile(@Part filePart: MultipartBody.Part): UploadResponseDto

    @POST(value = "/offer/create")
    suspend fun createOffer(
        @Body body: CreateOfferRequest
    )

    @GET(value = "/category")
    suspend fun getCategories(
        @Query("pageSize") pageSize: Int,
        @Query("pageNumber") pageNumber: Int,
    ): CategoryListDto

    @GET(value = "/category/for-user")
    suspend fun getCategoriesForUser(
        @Query("pageSize") pageSize: Int,
        @Query("pageNumber") pageNumber: Int,
    ): CategoryListDto

    @GET(value = "/chats")
    suspend fun resolveUserChats(): ChatListDto

    @GET(value = "/chats/{id}")
    suspend fun resolveChatMessagesById(
        @Path("id") recipientId: Long,
        @Query("size") pageSize: Int,
        @Query("page") pageNumber: Int,
        @Query("paged") paged: Boolean = true
    ): ChatHistoryDto
}