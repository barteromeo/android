package ru.barteromeo.data

import okhttp3.MultipartBody
import ru.barteromeo.data.dto.AuthDto
import ru.barteromeo.data.dto.ConfirmEmailDto
import ru.barteromeo.data.dto.LocationDto
import ru.barteromeo.data.dto.UploadResponseDto
import ru.barteromeo.data.dto.UserDto
import ru.barteromeo.data.network.AuthApi
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.data.requests.ConfirmEmailRequest
import ru.barteromeo.data.requests.CreateOfferRequest
import ru.barteromeo.data.requests.LoginRequest
import ru.barteromeo.data.requests.PasswordRecoverConfirmRequest
import ru.barteromeo.data.requests.PasswordRecoverNewRequest
import ru.barteromeo.data.requests.PasswordRecoverRequest
import ru.barteromeo.data.requests.RegisterRequest
import ru.barteromeo.data.requests.RequestEmailConfirmRequest
import ru.barteromeo.data.requests.UserInfoRequest
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSource @Inject constructor(
    private val baseApi: BaseApi,
    private val authApi: AuthApi
) {
    suspend fun getOfferById(id: Long) = baseApi.resolveOfferById(id)

    suspend fun getUserById(id: Long) = baseApi.getUserInfoById(id)

    suspend fun getCurrentUserInfo() = baseApi.getUserInfo()

    suspend fun uploadPhoto(multipartBody: MultipartBody.Part): UploadResponseDto =
        baseApi.uploadFile(multipartBody)

    suspend fun login(username: String, password: String) = authApi.login(
        LoginRequest(
            login = username,
            password = password
        )
    )

    suspend fun updateUserInfo(userDto: UserDto) =
        baseApi.changeUserInfo(userDto.toUpdateRequest())

    suspend fun updateUserPhone(phone: String) =
        baseApi.changeUserInfo(UserInfoRequest(phone = phone))

    suspend fun updateUserName(name: String, surname: String) = baseApi.changeUserInfo(
        UserInfoRequest(name = name, surname = surname)
    )

    suspend fun passwordRecoveryRequest(email: String) =
        authApi.passwordRecoveryRequest(
            PasswordRecoverRequest(
                email = email
            )
        )

    suspend fun passwordRecoveryConfirmRequest(email: String, code: String) =
        authApi.passwordRecoveryConfirm(
            PasswordRecoverConfirmRequest(
                email = email,
                code = code
            )
        )

    suspend fun passwordRecover(email: String, code: String, newPassword: String) =
        authApi.passwordRecovery(
            PasswordRecoverNewRequest(
                email = email,
                code = code,
                newPassword = newPassword
            )
        )


    suspend fun register(
        email: String,
        password: String,
        firstName: String,
        surname: String,
        code: String
    ) =
        authApi.register(
            RegisterRequest(
                login = email,
                email = email,
                phone = "",
                password = password,
                surname = surname,
                name = firstName,
                code = code,
            )
        )

    suspend fun refreshToken(token: String): AuthDto {
        return authApi.login(token)
    }

    suspend fun createOffer(
        title: String,
        photoIds: List<Int>,
        description: String,
        changeOn: String,
        phone: String,
        whereToMeet: String,
        tags: List<String>,
        type: String,
        categoryIds: List<Long>,
    ) {
        baseApi.createOffer(
            CreateOfferRequest(
                title = title,
                description = description,
                photoIds = photoIds,
                changeOn = changeOn,
                phone = phone,
                locations = listOf(LocationDto(null, null, address = whereToMeet, null, null)),
                categoryIds = categoryIds,
                tags = tags,
                type = type,
            )
        )
    }

    suspend fun addToWishlist(id: Long) {
        baseApi.addToWishlist(id)
    }

    suspend fun removeFromWishlist(id: Long) {
        baseApi.deleteFromWishlist(id)
    }

    suspend fun requestEmailCode(email: String) {
        authApi.requestEmailConfirmation(RequestEmailConfirmRequest(email))
    }

    suspend fun confirmEmail(email: String, code: String): ConfirmEmailDto {
        return authApi.confirmEmail(ConfirmEmailRequest(email, code))
    }
}