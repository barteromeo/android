package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AuthorDto(
    @SerializedName("id") val userId: Long?,
    @SerializedName("name") val name: String?,
    @SerializedName("photo") val photo: String?,
    @SerializedName("surname") val surname: String?,
    @SerializedName("phone") val phone: String?,
) : Serializable {
    companion object {
        private const val serialVersionUID = 2L
    }
}