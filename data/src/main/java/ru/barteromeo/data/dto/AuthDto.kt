package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AuthDto(
    @SerializedName("id") val accountId: Int?,
    @SerializedName("refreshToken") val refreshToken: String?,
    @SerializedName("accessToken") val accessToken: String?
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 2L
    }
}