package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CategoryDto(
    @SerializedName("name") val name: String?,
    @SerializedName("id") val id: Long?,
    @SerializedName("photo") val photo: String?,
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 1L
    }
}