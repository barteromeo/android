package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName

enum class OfferStatusDto {
    @SerializedName("OPEN")
    OPEN,
    @SerializedName("WITHDRAWN")
    WITHDRAWN,
    @SerializedName("TRADED")
    TRADED,
    @SerializedName("DELETED")
    DELETED;
}