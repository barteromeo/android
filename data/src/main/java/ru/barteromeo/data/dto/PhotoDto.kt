package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PhotoDto(
    @SerializedName("url") val photoUrl: String?,
    @SerializedName("id") val id: Long,
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}