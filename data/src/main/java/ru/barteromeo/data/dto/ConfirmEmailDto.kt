package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ConfirmEmailDto(
    @SerializedName("tokenIsValid") val tokenIsValid: Boolean,
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}