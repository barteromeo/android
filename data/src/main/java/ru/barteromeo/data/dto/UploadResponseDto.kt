package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UploadResponseDto(
    @SerializedName("id") val id: String
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}