package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import ru.barteromeo.data.requests.UserInfoRequest
import java.io.Serializable
import java.util.*

data class UserDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("login") val login: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("surname") val surname: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("gender") val gender: String?,
    @SerializedName("birth") val birth: Date?,
    @SerializedName("about") val about: String?,
    @SerializedName("register") val register: Date?,
    @SerializedName("photo") val photo: PhotoDto?,
    @SerializedName("location") val location: LocationDto?,
    @SerializedName("campus") val campus: String?,
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 2L
    }

    fun toUpdateRequest(): UserInfoRequest {
        return UserInfoRequest(
            id = id,
            login = login,
            phone = phone,
            email = email,
            surname = surname,
            name = name,
            gender = gender,
            birth = birth,
            about = about,
            register = register,
            photoId = photo?.id
        )
    }
}
