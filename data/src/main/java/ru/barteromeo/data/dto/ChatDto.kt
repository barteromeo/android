package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChatDto(
    @SerializedName("id") val id: Long?,
    @SerializedName("lastMessage") val lastMessage: MessageDto?,
    @SerializedName("unread") val messagesUnread: Long?,
    @SerializedName("user") val chatUser: AuthorDto?,
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

}