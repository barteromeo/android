package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TagDto(
    @SerializedName("id") val id: Int,
    @SerializedName("tag") val tag: String,
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 1L
    }
}