package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CategoryListDto(
    @SerializedName("data") val categories: List<CategoryDto>,
    @SerializedName("pageSize") val pageSize: Int,
    @SerializedName("pageNum") val pageNum: Int,
    @SerializedName("totalElements") val totalElements: Int
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}