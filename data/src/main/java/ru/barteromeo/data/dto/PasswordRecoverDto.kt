package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PasswordRecoverDto(
    @SerializedName("tokenIsValid") val tokenIsValid: Boolean
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 1L
    }
}