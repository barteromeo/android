package ru.barteromeo.data.dto

enum class OfferTypeDto {
    PRODUCT,
    SERVICE,
    UNKNOWN;
}