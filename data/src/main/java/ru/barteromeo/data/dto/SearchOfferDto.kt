package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SearchOfferDto(
    @SerializedName("id") val id: Long?,
    @SerializedName("title") val title: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("photo") val photo: PhotoDto?,
    @SerializedName("views") val views: Int?,
    @SerializedName("state") val offerStatusDto: OfferStatusDto?,
    @SerializedName("type") val offerTypeDto: OfferTypeDto?,
    @SerializedName("author") val author: AuthorDto?,
    @SerializedName("categories") val categories: List<CategoryDto>?,
    @SerializedName("locations") val locations: List<LocationDto>?,
    @SerializedName("inWishList") val inWishlist: Boolean?,
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 3L
    }
}