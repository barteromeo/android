package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

data class MessageDto(
    @SerializedName("attachments") val attachments: List<AttachmentDto>?,
    @SerializedName("chatId") val chatId: Long?,
    @SerializedName("date") val date: String?,
    @SerializedName("edited") val edited: Boolean?,
    @SerializedName("id") val messageId: Long?,
    @SerializedName("message") val message: String?,
    @SerializedName("userId") val userId: Long?,
): Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

}