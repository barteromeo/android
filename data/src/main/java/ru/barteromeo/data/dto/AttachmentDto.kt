package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AttachmentDto(
    @SerializedName("id") val id: Long?,
    @SerializedName("type") val type: String?,
    @SerializedName("url") val url: String?,
): Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

}