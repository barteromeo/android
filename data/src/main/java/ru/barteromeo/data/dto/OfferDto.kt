package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

data class OfferDto(
    @SerializedName("id") val id: Long?,
    @SerializedName("title") val title: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("photos") val photos: List<PhotoDto>?,
    @SerializedName("views") val views: Int?,
    @SerializedName("state") val offerStatusDto: OfferStatusDto?,
    @SerializedName("type") val offerTypeDto: OfferTypeDto?,
    @SerializedName("author") val author: AuthorDto?,
    @SerializedName("categories") val categories: List<CategoryDto>?,
    @SerializedName("locations") val locations: List<LocationDto>?,
    @SerializedName("updated") val updated: Date?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("changeOn") val changeOn: String?,
    @SerializedName("inWishList") val inWishlist: Boolean?,
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 5L
    }
}