package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChatListDto(
    @SerializedName("chats") val chats: List<ChatDto>
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

}