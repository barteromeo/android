package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MessageListDto(
    @SerializedName("data") val messages: List<MessageDto>,
    @SerializedName("pageSize") val pageSize: Int,
    @SerializedName("pageNum") val pageNum: Int,
    @SerializedName("totalElements") val totalElements: Int
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

}