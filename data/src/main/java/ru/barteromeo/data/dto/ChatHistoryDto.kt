package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ChatHistoryDto(
    @SerializedName("userInfo") val user: AuthorDto?,
    @SerializedName("messages") val messageListDto: MessageListDto,
    @SerializedName("id") val chatId: Long?,
): Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

}