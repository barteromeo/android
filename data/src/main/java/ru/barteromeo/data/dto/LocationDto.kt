package ru.barteromeo.data.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LocationDto(
    @SerializedName("longitude") val longitude: Float?,
    @SerializedName("latitude") val latitude: Float?,
    @SerializedName("address") val address: String?,
    @SerializedName("metro") val metro: String?,
    @SerializedName("city") val city: String?,
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 3L
    }
}