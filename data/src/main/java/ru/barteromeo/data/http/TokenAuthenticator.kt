package ru.barteromeo.data.http

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import ru.barteromeo.data.UserPreferences
import ru.barteromeo.data.dto.AuthDto
import ru.barteromeo.data.network.AuthApi
import ru.barteromeo.util.data.Resource
import ru.barteromeo.data.network.safeApiCall

class TokenAuthenticator(
    private val authApi: AuthApi,
    private val userPreferences: UserPreferences
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        return runBlocking {
            when (val tokenResponse = getUpdatedToken()) {
                is Resource.Success -> {
                    val accessToken = tokenResponse.value.accessToken
                    val refreshToken = tokenResponse.value.refreshToken
                    if (accessToken == null) {
                        return@runBlocking null
                    }
                    userPreferences.updateTokens(
                        accessToken,
                        refreshToken
                    )
                    response.request.newBuilder()
                        .header("Authorization", "${tokenResponse.value.accessToken}")
                        .build()
                }
                else -> {
                    userPreferences.clear()
                    null
                }
            }
        }
    }

    private suspend fun getUpdatedToken(): Resource<AuthDto> {
        val refreshToken = userPreferences.refreshToken.first()
        return safeApiCall { authApi.login(refreshToken = refreshToken) }
    }
}