package ru.barteromeo.data.http

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import ru.barteromeo.data.UserPreferences
import javax.inject.Inject

interface AuthTokenInterceptor : Interceptor

class AuthTokenInterceptorImpl @Inject constructor(private val userPreferences: UserPreferences) :
    AuthTokenInterceptor {
    override fun intercept(chain: Interceptor.Chain): Response = runBlocking {
        val accessToken = userPreferences.accessToken.first()
        if (accessToken != null) {
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "${userPreferences.accessToken.first()}").build()
            return@runBlocking chain.proceed(request)
        }
        return@runBlocking chain.proceed(chain.request())
    }
}