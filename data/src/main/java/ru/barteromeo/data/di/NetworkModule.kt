package ru.barteromeo.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.barteromeo.data.http.AuthTokenInterceptor
import ru.barteromeo.data.http.AuthTokenInterceptorImpl
import ru.barteromeo.data.http.ConnectivityInterceptor
import ru.barteromeo.data.http.ConnectivityInterceptorImpl
import ru.barteromeo.data.network.AuthApi
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.data.network.RemoteHelper
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providesAuthApi(
        remoteHelper: RemoteHelper,
    ): AuthApi {
        return remoteHelper.buildAuthApi()
    }

    @Singleton
    @Provides
    fun providesBaseApi(
        remoteHelper: RemoteHelper,
    ): BaseApi {
        return remoteHelper.buildApi(BaseApi::class.java)
    }

    @Provides
    @Singleton
    fun providesConnectiviryInterceptor(connectivityInterceptor: ConnectivityInterceptorImpl):
            ConnectivityInterceptor {
        return connectivityInterceptor
    }

    @Provides
    @Singleton
    fun providesAuthtokenInterceptor(authTokenInterceptorImpl: AuthTokenInterceptorImpl): AuthTokenInterceptor {
        return authTokenInterceptorImpl
    }

//    @Provides
//    @Singleton
//    fun providesUserPreferences(@ApplicationContext context: Context): UserPreferences {
//        return UserPreferences(context)
//    }

}