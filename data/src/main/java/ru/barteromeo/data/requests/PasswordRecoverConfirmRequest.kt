package ru.barteromeo.data.requests

import java.io.Serializable

class PasswordRecoverConfirmRequest (
    val email: String,
    val code: String,
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}