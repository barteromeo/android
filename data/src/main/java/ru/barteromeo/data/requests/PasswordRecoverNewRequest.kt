package ru.barteromeo.data.requests

import java.io.Serializable

class PasswordRecoverNewRequest (
    val email: String,
    val code: String,
    val newPassword: String
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}