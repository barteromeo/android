package ru.barteromeo.data.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SearchRequest(
    @SerializedName("categories") val categories: List<Int>? = null,
    @SerializedName("ids") val ids: List<Int>? = null,
    @SerializedName("tags") val tags: List<String>? = null,
    @SerializedName("request") val request: String? = null,
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}