package ru.barteromeo.data.requests

import com.google.gson.annotations.SerializedName
import ru.barteromeo.data.dto.LocationDto
import java.io.Serializable

data class CreateOfferRequest(
    @SerializedName("categories") private val categoryIds: List<Long>,
    @SerializedName("changeOn") private val changeOn: String,
    @SerializedName("description") private val description: String,
    @SerializedName("locations") private val locations: List<LocationDto>,
    @SerializedName("phone") private val phone: String,
    @SerializedName("photoIds") private val photoIds: List<Int>,
    @SerializedName("tags") private val tags: List<String>,
    @SerializedName("title") private val title: String,
    @SerializedName("type") private val type: String,
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}