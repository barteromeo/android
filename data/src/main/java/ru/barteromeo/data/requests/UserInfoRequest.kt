package ru.barteromeo.data.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

data class UserInfoRequest(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("login") val login: String? = null,
    @SerializedName("phone") val phone: String? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("surname") val surname: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("gender") val gender: String? = null,
    @SerializedName("birth") val birth: Date? = null,
    @SerializedName("about") val about: String? = null,
    @SerializedName("register") val register: Date? = null,
    @SerializedName("photoId") val photoId: Long? = null
) : Serializable {
    companion object {
        @JvmStatic
        val serialVersionUID = 1L
    }
}