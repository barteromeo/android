package ru.barteromeo.data.requests

import java.io.Serializable

data class PasswordRecoverRequest (
    val email: String
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}