package ru.barteromeo.data.requests

import java.io.Serializable

data class LoginRequest(
    val login: String,
    val password: String
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}