package ru.barteromeo.data.requests

import com.google.gson.annotations.SerializedName

data class RegisterRequest(
    @SerializedName("login") val login: String,
    @SerializedName("email") val email: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("password") val password: String,
    @SerializedName("surname") val surname: String,
    @SerializedName("name") val name: String,
    @SerializedName("code") val code: String,
)