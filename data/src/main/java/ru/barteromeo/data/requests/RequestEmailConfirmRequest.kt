package ru.barteromeo.data.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class RequestEmailConfirmRequest(
    @SerializedName("email") val email: String
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}