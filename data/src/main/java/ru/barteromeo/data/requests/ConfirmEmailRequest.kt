package ru.barteromeo.data.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ConfirmEmailRequest(
    @SerializedName("email") val email: String,
    @SerializedName("code") val code: String,
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}