package ru.barteromeo.data.client.offer

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import ru.barteromeo.data.dto.CategoryDto
import ru.barteromeo.data.network.BaseApi
import java.io.IOException
import javax.inject.Inject

class CategoryClient @Inject constructor(private val api: BaseApi) :
    PagingSource<Int, CategoryDto>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CategoryDto> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response =
                api.getCategories(params.loadSize, page)
            val categories = response.categories
            LoadResult.Page(
                data = categories,
                prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                nextKey = if (response.totalElements < params.loadSize) null else page + 1
            )

        } catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, CategoryDto>): Int? {
        return state.anchorPosition
    }

    companion object {
        const val STARTING_PAGE_INDEX = 1
    }
}