package ru.barteromeo.data.client.offer

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import ru.barteromeo.data.dto.SearchOfferDto
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.data.requests.SearchRequest
import java.io.IOException

class OfferClient constructor(private val api: BaseApi, private val request: String?) :
    PagingSource<Int, SearchOfferDto>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SearchOfferDto> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response =
                api.resolveOffers(
                    SearchRequest(request = request),
                    10,
                    page
                )
            val offers = response.offers
            LoadResult.Page(
                data = offers,
                prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                nextKey = if (response.totalElements < 10 * page) null else page + 1
            )

        } catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, SearchOfferDto>): Int? {
        return state.anchorPosition
    }

    companion object {
        const val STARTING_PAGE_INDEX = 1
    }
}