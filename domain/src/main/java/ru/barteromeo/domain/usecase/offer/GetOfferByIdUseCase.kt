package ru.barteromeo.domain.usecase.offer

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Offer
import ru.barteromeo.domain.model.SearchOffer
import ru.barteromeo.domain.repository.OfferRepository
import javax.inject.Inject

@Reusable
class GetOfferByIdUseCase @Inject constructor(private val offersRepository: OfferRepository) {

    suspend fun execute(offerId: Long): Flow<Offer> {
        return offersRepository.resolveOfferById(offerId)
    }

}