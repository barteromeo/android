package ru.barteromeo.domain.usecase.user

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.User
import ru.barteromeo.domain.repository.UserRepository
import javax.inject.Inject

@Reusable
class GetUserByIdUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend fun execute(id: Long): Flow<User?> {
        return userRepository.getUserById(id)
    }

}