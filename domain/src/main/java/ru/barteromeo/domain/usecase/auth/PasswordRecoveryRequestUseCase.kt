package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class PasswordRecoveryRequestUseCase @Inject constructor(private val authRepository: AuthRepository) {

    suspend fun execute(
        email: String,
    ): Flow<Boolean> {
        return authRepository.passwordRecoveryRequest(email)
    }

}