package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.UserPreferences
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class CheckAuthPresentUseCase @Inject constructor(
    private val userPreferences: UserPreferences,
    private val authRepository: AuthRepository
) {

    suspend fun execute(): Flow<Boolean> {
        return flow {
            userPreferences.refreshToken.collect {
                if (it != null) {
                    authRepository.refreshToken(it).collect {
                        emit(it)
                    }
                } else {
                    emit(false)
                }
            }
        }
    }

}