package ru.barteromeo.domain.usecase.offer

import androidx.paging.PagingData
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.SearchOffer
import ru.barteromeo.domain.repository.OfferRepository
import javax.inject.Inject

@Reusable
class GetOffersUseCase @Inject constructor(private val offerRepository: OfferRepository) {

    suspend fun execute(request: String?): Flow<PagingData<SearchOffer>> {
        return offerRepository.resolveOffers(request)
    }

}