package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class RegisterUseCase @Inject constructor(private val authRepository: AuthRepository) {

    suspend fun execute(
        email: String,
        password: String,
        firstName: String,
        surname: String,
        code: String
    ): Flow<Boolean> {
        return authRepository.register(email, password, firstName, surname, code)
    }
}

