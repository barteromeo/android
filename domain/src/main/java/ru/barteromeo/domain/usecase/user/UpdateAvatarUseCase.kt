package ru.barteromeo.domain.usecase.user

import dagger.Reusable
import ru.barteromeo.domain.repository.UserRepository
import javax.inject.Inject

@Reusable
class UpdateAvatarUseCase @Inject constructor(private val userRepository: UserRepository) {

    suspend fun execute(id: Long) {
        userRepository.updateUserPhoto(id)
    }

}