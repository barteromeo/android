package ru.barteromeo.domain.usecase.user

import dagger.Reusable
import ru.barteromeo.domain.repository.UserRepository
import javax.inject.Inject

@Reusable
class UpdatePhoneUseCase @Inject constructor(private val userRepository: UserRepository) {

    suspend fun execute(phone: String) {
        userRepository.updateUserPhone(phone = phone)
    }

}