package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class ConfirmEmailCodeUseCase @Inject constructor(private val authRepository: AuthRepository) {

    suspend fun execute(code: String, email: String): Flow<Boolean> {
        return authRepository.sendConfirmEmailCode(email, code)
    }

}