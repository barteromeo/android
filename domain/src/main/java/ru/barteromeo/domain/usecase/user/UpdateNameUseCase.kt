package ru.barteromeo.domain.usecase.user

import dagger.Reusable
import ru.barteromeo.domain.repository.UserRepository
import javax.inject.Inject

@Reusable
class UpdateNameUseCase @Inject constructor(private val userRepository: UserRepository) {

    suspend fun execute(name: String, surname: String) {
        userRepository.updateUserName(name, surname)
    }

}