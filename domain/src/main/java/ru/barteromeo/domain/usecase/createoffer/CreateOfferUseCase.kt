package ru.barteromeo.domain.usecase.createoffer

import dagger.Reusable
import ru.barteromeo.domain.model.OfferType
import ru.barteromeo.domain.repository.OfferRepository
import javax.inject.Inject

@Reusable
class CreateOfferUseCase @Inject constructor(private val offerRepository: OfferRepository) {

    suspend fun execute(
        title: String,
        photoIds: List<Int>,
        description: String,
        changeOn: String,
        phone: String,
        whereToMeet: String,
        tags: List<String>,
        type: OfferType,
        categoryIds: List<Long>,
    ) {
        offerRepository.createOffer(
            title = title,
            photoIds = photoIds,
            description = description,
            changeOn = changeOn,
            phone = phone,
            whereToMeet = whereToMeet,
            tags = tags,
            type = type,
            categoryIds = categoryIds,
        )
    }

}