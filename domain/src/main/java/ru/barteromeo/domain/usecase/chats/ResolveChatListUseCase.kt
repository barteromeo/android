package ru.barteromeo.domain.usecase.chats

import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Chat
import ru.barteromeo.domain.repository.ChatsRepository
import javax.inject.Inject

class ResolveChatListUseCase @Inject constructor(private val chatsRepository: ChatsRepository) {

    suspend fun execute(): Flow<List<Chat>> {
        return chatsRepository.resolveChats()
    }

}