package ru.barteromeo.domain.usecase.category

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Category
import ru.barteromeo.domain.repository.CategoryRepository
import javax.inject.Inject

@Reusable
class GetCategoriesUseCase @Inject constructor(
    private val categoryRepository: CategoryRepository
) {

    suspend fun execute(): Flow<List<Category>> {
        return categoryRepository.getAllCategories()
    }

}