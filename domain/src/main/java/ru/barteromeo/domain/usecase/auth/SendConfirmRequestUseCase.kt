package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class SendConfirmRequestUseCase @Inject constructor(
    private val authRepository: AuthRepository
) {

    suspend fun execute(email: String) {
        authRepository.requestConfirmEmailCode(email)
    }

}