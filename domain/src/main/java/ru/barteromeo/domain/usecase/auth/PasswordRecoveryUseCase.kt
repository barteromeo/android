package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class PasswordRecoveryUseCase @Inject constructor(private val authRepository: AuthRepository) {

    suspend fun execute(
        email: String,
        code: String,
        newPassword: String
    ): Flow<Boolean> {
        return authRepository.passwordRecover(email, code, newPassword)
    }

}