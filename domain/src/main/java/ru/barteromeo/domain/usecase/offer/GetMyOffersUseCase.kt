package ru.barteromeo.domain.usecase.offer

import androidx.paging.PagingData
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.SearchOffer
import ru.barteromeo.domain.repository.MyOffersRepository
import javax.inject.Inject

@Reusable
class GetMyOffersUseCase @Inject constructor(private val offerRepository: MyOffersRepository) {

    suspend fun execute(): Flow<PagingData<SearchOffer>> {
        return offerRepository.resolveOffers()
    }

}