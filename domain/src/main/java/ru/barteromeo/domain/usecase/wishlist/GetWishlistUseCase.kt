package ru.barteromeo.domain.usecase.wishlist

import androidx.paging.PagingData
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.SearchOffer
import ru.barteromeo.domain.repository.WishlistRepository
import javax.inject.Inject


@Reusable
class GetWishlistUseCase @Inject constructor(private val wishlistRepository: WishlistRepository) {

    suspend fun execute(): Flow<PagingData<SearchOffer>> {
        return wishlistRepository.resolveOffers()
    }

}