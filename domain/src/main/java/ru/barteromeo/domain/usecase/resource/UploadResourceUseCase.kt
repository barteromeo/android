package ru.barteromeo.domain.usecase.resource

import android.net.Uri
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.data.dto.UploadResponseDto
import ru.barteromeo.domain.repository.ResourceRepository
import javax.inject.Inject

@Reusable
class UploadResourceUseCase @Inject constructor(
    private val resourceRepository: ResourceRepository
) {

    suspend fun execute(uri: Uri): Flow<UploadResponseDto> {
        return resourceRepository.uploadPhoto(uri)
    }

}