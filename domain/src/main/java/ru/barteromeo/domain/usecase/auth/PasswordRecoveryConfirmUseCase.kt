package ru.barteromeo.domain.usecase.auth

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.repository.AuthRepository
import javax.inject.Inject

@Reusable
class PasswordRecoveryConfirmUseCase @Inject constructor(private val authRepository: AuthRepository) {

    suspend fun execute(
        email: String,
        code: String
    ): Flow<Boolean> {
        return authRepository.passwordRecoveryConfirmRequest(email, code)
    }

}