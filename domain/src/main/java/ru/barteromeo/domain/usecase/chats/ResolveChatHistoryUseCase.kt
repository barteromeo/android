package ru.barteromeo.domain.usecase.chats

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Message
import ru.barteromeo.domain.repository.ChatsRepository
import javax.inject.Inject

class ResolveChatHistoryUseCase @Inject constructor(private val chatsRepository: ChatsRepository) {

    suspend fun resolveChatHistoryUseCase(recipientId: Long): Flow<PagingData<Message>> {
        return chatsRepository.resolveChatHistory(recipientId)
    }

}