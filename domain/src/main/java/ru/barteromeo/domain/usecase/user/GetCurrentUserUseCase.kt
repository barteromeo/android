package ru.barteromeo.domain.usecase.user

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.User
import ru.barteromeo.domain.repository.UserRepository
import javax.inject.Inject

@Reusable
class GetCurrentUserUseCase @Inject constructor(private val userRepository: UserRepository) {

    suspend fun execute(forceUpdate: Boolean = false): Flow<User?> {
        return userRepository.getCurrentUser(forceUpdate)
    }

}