package ru.barteromeo.domain.usecase.wishlist

import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.repository.WishlistRepository
import ru.barteromeo.util.data.Resource
import javax.inject.Inject

@Reusable
class AddToWishlistUseCase @Inject constructor(
    private val wishlistRepository: WishlistRepository
) {

    suspend fun execute(id: Long, inWishlist: Boolean): Flow<Resource<*>> {
        return if (inWishlist) {
            wishlistRepository.removeFromWishlist(id)
        } else {
            wishlistRepository.addToWishlist(id)
        }
    }

}