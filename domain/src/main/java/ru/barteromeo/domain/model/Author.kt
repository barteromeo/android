package ru.barteromeo.domain.model

data class Author(
    val id: Long,
    val photo: String?,
    val name: String,
    val surname: String,
    val phone: String?,
)