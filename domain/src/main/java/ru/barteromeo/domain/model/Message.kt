package ru.barteromeo.domain.model

import java.util.*

data class Message(
    val attachments: List<Attachment>,
    val chatId: Long?,
    val date: Date?,
    val edited: Boolean?,
    val messageId: Long?,
    val message: String?,
    val userId: Long?,
)