package ru.barteromeo.domain.model

data class SearchOffer(
    val id: Long,
    val user: Author,
    val title: String,
    val description: String,
    val photos: List<String>,
    val views: Int,
    val offerStatus: OfferStatus,
    val offerType: OfferType,
    val categories: List<Category>,
    val location: Location,
    val inWishlist: Boolean,
)
