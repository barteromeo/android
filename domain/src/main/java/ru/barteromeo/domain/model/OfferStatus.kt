package ru.barteromeo.domain.model

enum class OfferStatus {
    OPEN,
    TRADED,
    WITHDRAWN,
    DELETED;
}