package ru.barteromeo.domain.model

data class Location(
    val address: String,
    val city: String,
    val metro: String,
)