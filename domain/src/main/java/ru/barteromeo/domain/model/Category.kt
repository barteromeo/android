package ru.barteromeo.domain.model

data class Category(
    val id: Long,
    val value: String,
    val photo: String?,
)