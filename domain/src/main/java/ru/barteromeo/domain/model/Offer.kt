package ru.barteromeo.domain.model

import java.util.*

data class Offer(
    val id: Long,
    val user: Author,
    val title: String,
    val description: String,
    val photos: List<String>,
    val views: Int,
    val offerStatus: OfferStatus,
    val offerType: OfferType,
    val categories: List<Category>,
    val location: Location,
    val updated: Date,
    val phone: String,
    val changeOn: String,
    val inWishlist: Boolean,
)
