package ru.barteromeo.domain.model

data class Chat(
    val id: Long,
    val lastMessage: Message?,
    val messagesUnread: Long,
    val chatUser: Author,
)