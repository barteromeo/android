package ru.barteromeo.domain.model

data class Attachment(
    val id: Long,
    val type: String,
    val url: String,
)