package ru.barteromeo.domain.model

enum class OfferType(val type: String) {
    GOOD("Товар"),
    SERVICE("Услуга"),
    UNKNOWN("Другое");
}