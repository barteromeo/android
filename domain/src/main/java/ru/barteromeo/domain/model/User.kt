package ru.barteromeo.domain.model

data class User(
    val id: Int,
    val login: String,
    val phone: String?,
    val email: String,
    val surname: String,
    val name: String,
    val gender: String,
    val about: String,
    val photo: String?
)