package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.AttachmentDto
import ru.barteromeo.domain.model.Attachment
import javax.inject.Inject

@Reusable
class AttachmentMapper @Inject constructor() {

    fun map(attachmentDto: AttachmentDto): Attachment {
        return Attachment(
            attachmentDto.id!!,
            attachmentDto.type!!,
            attachmentDto.url!!
        )
    }

}