package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.OfferDto
import ru.barteromeo.data.dto.OfferStatusDto
import ru.barteromeo.data.dto.OfferTypeDto
import ru.barteromeo.data.dto.SearchOfferDto
import ru.barteromeo.domain.model.Offer
import ru.barteromeo.domain.model.SearchOffer
import java.util.*
import javax.inject.Inject

@Reusable
class OfferMapper @Inject constructor(
    private val authorMapper: AuthorMapper,
    private val tagMapper: TagMapper,
    private val offerTypeMapper: OfferTypeMapper,
    private val offerStatusMapper: OfferStatusMapper,
    private val locationMapper: LocationMapper,
) {

    fun mapOffer(offerDto: OfferDto): Offer {
        return Offer(
            id = offerDto.id ?: 0,
            user = offerDto.author?.let { authorMapper.map(it) }
                ?: throw NullPointerException("OfferMapper: offerDto author is null"),
            title = offerDto.title.orEmpty(),
            description = offerDto.description.orEmpty(),
            photos = offerDto.photos?.mapNotNull { it.photoUrl } ?: emptyList(),
            views = offerDto.views ?: 0,
            offerStatus = offerStatusMapper.map(offerDto.offerStatusDto ?: OfferStatusDto.DELETED),
            offerType = offerTypeMapper.map(offerDto.offerTypeDto ?: OfferTypeDto.UNKNOWN),
            categories = offerDto.categories?.map { tagMapper.mapFromCategoryDto(it) }
                ?: emptyList(),
            location = locationMapper.mapLocation(offerDto.locations?.first()),
            updated = offerDto.updated ?: Date(),
            phone = offerDto.phone ?: offerDto.author?.phone
            ?: throw NullPointerException("No phone provided"),
            changeOn = offerDto.changeOn.orEmpty(),
            inWishlist = offerDto.inWishlist ?: false,
        )
    }

    fun mapOffer(offerDto: SearchOfferDto): SearchOffer {
        return SearchOffer(
            id = offerDto.id ?: 0,
            user = offerDto.author?.let { authorMapper.map(it) }
                ?: throw NullPointerException("OfferMapper: offerDto author is null"),
            title = offerDto.title.orEmpty(),
            description = offerDto.description.orEmpty(),
            photos = listOfNotNull(offerDto.photo?.photoUrl),
            views = offerDto.views ?: 0,
            offerStatus = offerStatusMapper.map(offerDto.offerStatusDto ?: OfferStatusDto.DELETED),
            offerType = offerTypeMapper.map(offerDto.offerTypeDto ?: OfferTypeDto.UNKNOWN),
            categories = offerDto.categories?.map { tagMapper.mapFromCategoryDto(it) }
                ?: emptyList(),
            location = locationMapper.mapLocation(offerDto.locations?.first()),
            inWishlist = offerDto.inWishlist ?: false
        )
    }

}