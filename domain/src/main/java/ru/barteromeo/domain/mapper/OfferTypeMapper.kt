package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.OfferTypeDto
import ru.barteromeo.domain.model.OfferType
import javax.inject.Inject

@Reusable
class OfferTypeMapper @Inject constructor() {

    fun map(offerTypeDto: OfferTypeDto): OfferType {
        return when (offerTypeDto) {
            OfferTypeDto.PRODUCT -> OfferType.GOOD
            OfferTypeDto.SERVICE -> OfferType.SERVICE
            OfferTypeDto.UNKNOWN -> OfferType.UNKNOWN
        }
    }

}