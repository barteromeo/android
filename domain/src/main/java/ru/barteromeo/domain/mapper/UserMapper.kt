package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.UserDto
import ru.barteromeo.domain.model.User
import javax.inject.Inject

@Reusable
class UserMapper @Inject constructor() {

    fun map(userDto: UserDto?): User? {
        return userDto?.let { userDto ->
            User(
                id = userDto.id ?: throw NullPointerException("UserMapper: userDto id is null"),
                login = userDto.login
                    ?: throw NullPointerException("UserMapper: userDto login is null"),
                phone = userDto.phone,
                email = userDto.email
                    ?: throw NullPointerException("UserMapper: userDto email is null"),
                surname = userDto.surname
                    ?: throw NullPointerException("UserMapper: userDto surname is null"),
                name = userDto.name
                    ?: throw NullPointerException("UserMapper: userDto name is null"),
                gender = userDto.gender
                    ?: throw NullPointerException("UserMapper: userDto gender is null"),
                about = userDto.about.orEmpty(),
                photo = userDto.photo?.photoUrl
            )
        }
    }

}