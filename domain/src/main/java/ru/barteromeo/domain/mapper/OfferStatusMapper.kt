package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.OfferStatusDto
import ru.barteromeo.domain.model.OfferStatus
import javax.inject.Inject

@Reusable
class OfferStatusMapper @Inject constructor() {

    fun map(offerStatusDto: OfferStatusDto): OfferStatus {
        return when (offerStatusDto) {
            OfferStatusDto.OPEN -> OfferStatus.OPEN
            OfferStatusDto.TRADED -> OfferStatus.TRADED
            OfferStatusDto.WITHDRAWN -> OfferStatus.WITHDRAWN
            OfferStatusDto.DELETED -> OfferStatus.DELETED
        }
    }

}