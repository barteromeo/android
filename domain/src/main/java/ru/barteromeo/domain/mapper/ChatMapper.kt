package ru.barteromeo.domain.mapper

import ru.barteromeo.data.dto.ChatDto
import ru.barteromeo.domain.model.Chat
import javax.inject.Inject

class ChatMapper @Inject constructor(
    private val messageMapper: MessageMapper,
    private val authorMapper: AuthorMapper,
) {

    fun map(dto: ChatDto): Chat {
        return Chat(
            id = dto.id!!,
            lastMessage = dto.lastMessage?.let { messageMapper.map(it) },
            messagesUnread = dto.messagesUnread ?: 0L,
            chatUser = authorMapper.map(dto.chatUser!!)
        )
    }

}