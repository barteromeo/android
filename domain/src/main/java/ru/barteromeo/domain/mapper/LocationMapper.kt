package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.LocationDto
import ru.barteromeo.domain.model.Location
import javax.inject.Inject

@Reusable
class LocationMapper @Inject constructor() {

    fun mapLocation(locationDto: LocationDto?): Location {
        return Location(
            address = locationDto?.address.orEmpty(),
            metro = locationDto?.metro.orEmpty(),
            city = locationDto?.city.orEmpty(),
        )
    }

}