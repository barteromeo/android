package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.AuthorDto
import ru.barteromeo.domain.model.Author
import javax.inject.Inject

@Reusable
class AuthorMapper @Inject constructor() {

    fun map(authorDto: AuthorDto): Author {
        return Author(
            id = authorDto.userId
                ?: throw NullPointerException("AuthorMapper: authorDto id is null"),
            surname = authorDto.surname
                ?: "",// throw NullPointerException("AuthorMapper: authorDto surname is null"),
            name = authorDto.name
                ?: throw NullPointerException("AuthorMapper: authorDto name is null"),
            photo = authorDto.photo,
            phone = authorDto.phone,
        )
    }
}