package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.CategoryDto
import ru.barteromeo.domain.model.Category
import javax.inject.Inject

@Reusable
class CategoryMapper @Inject constructor() {

    fun map(categoryDto: CategoryDto) : Category {
        return Category(
            categoryDto.id!!,
            categoryDto.name!!,
            categoryDto.photo,
        )
    }

}