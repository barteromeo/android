package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.CategoryDto
import ru.barteromeo.data.dto.TagDto
import ru.barteromeo.domain.model.Category
import javax.inject.Inject

@Reusable
class TagMapper @Inject constructor() {

    fun map(tagDto: TagDto): Category {
        return Category(
            id = tagDto.id.toLong(),
            value = tagDto.tag,
            photo = null
        )
    }

    fun mapFromCategoryDto(categoryDto: CategoryDto): Category {
        return Category(
            id = categoryDto.id ?: throw NullPointerException("TagMapper: categoryDto id is null"),
            value = categoryDto.name
                ?: throw NullPointerException("TagMapper: categoryDto name is null"),
            photo = categoryDto.photo
        )
    }

}