package ru.barteromeo.domain.mapper

import dagger.Reusable
import ru.barteromeo.data.dto.MessageDto
import ru.barteromeo.domain.model.Message
import java.util.*
import javax.inject.Inject

@Reusable
class MessageMapper @Inject constructor(
    private val authorMapper: AuthorMapper,
    private val attachmentMapper: AttachmentMapper
) {

    fun map(dto: MessageDto): Message {
        return Message(
            attachments = dto.attachments?.map { attachmentMapper.map(it) } ?: emptyList(),
            chatId = dto.chatId,
            date = Date(),
            edited = dto.edited ?: false,
            message = dto.message,
            messageId = dto.messageId!!,
            userId = dto.userId!!,
        )
    }

    fun mapForLiveChats(dto: MessageDto): Message {
        return Message(
            attachments = emptyList(),
            chatId = null,
            date = Date(),
            message = dto.message,
            messageId = dto.messageId,
            userId = dto.userId,
            edited = false,
        )
    }

}