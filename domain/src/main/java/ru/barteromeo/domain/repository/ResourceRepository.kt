package ru.barteromeo.domain.repository

import android.net.Uri
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.Dispatcher
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.create
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody.Companion.toResponseBody
import ru.barteromeo.data.RemoteDataSource
import ru.barteromeo.data.dto.UploadResponseDto
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ResourceRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val imageCompressor: ImageCompressor,
) {

    suspend fun uploadPhoto(uri: Uri): Flow<UploadResponseDto> {
        return flow {
            val data = imageCompressor.compressImage(uri)
            val body = data?.toRequestBody("image/*".toMediaTypeOrNull())?.let {
                MultipartBody.Part.createFormData(
                    "file",
                    "filename",
                    it
                )
            }
            body?.let {
                emit(remoteDataSource.uploadPhoto(multipartBody = it))
            }
        }.flowOn(Dispatchers.IO)
    }

}