package ru.barteromeo.domain.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.client.chat.ChatsClient
import ru.barteromeo.data.dto.MessageDto
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.domain.mapper.ChatMapper
import ru.barteromeo.domain.mapper.MessageMapper
import ru.barteromeo.domain.model.Chat
import ru.barteromeo.domain.model.Message
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatsRepository @Inject constructor(
    private val baseApi: BaseApi,
    private val messageMapper: MessageMapper,
    private val chatMapper: ChatMapper,
) {

    suspend fun resolveChatHistory(recipientId: Long): Flow<PagingData<Message>> {
        return resolveChatHistoryFromNet(recipientId).map { pagedData ->
            pagedData.map {
                messageMapper.map(it)
            }
        }
    }

    private suspend fun resolveChatHistoryFromNet(recipientId: Long): Flow<PagingData<MessageDto>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = NETWORK_PAGE_SIZE),
            pagingSourceFactory = {
                ChatsClient(baseApi, recipientId)
            }
        ).flow
    }

    suspend fun resolveChats(): Flow<List<Chat>> {
        return flow {
            emit(baseApi.resolveUserChats().chats)
        }.map {
            it.map { chatMapper.map(it) }
        }
    }

    suspend fun sendMessage() {
        // todo
    }


    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }

}