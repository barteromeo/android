package ru.barteromeo.domain.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.RemoteDataSource
import ru.barteromeo.data.dto.PhotoDto
import ru.barteromeo.data.dto.UserDto
import ru.barteromeo.domain.mapper.UserMapper
import ru.barteromeo.domain.model.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val userMapper: UserMapper
) {

    private var currentUserFlow: MutableStateFlow<UserDto?> = MutableStateFlow(null)

    suspend fun getCurrentUser(forceUpdate: Boolean): Flow<User?> {
        if (forceUpdate || currentUserFlow.value == null) {
            fetchCurrentUser()
        }
        return currentUserFlow.map { userMapper.map(it) }
    }

    private suspend fun fetchCurrentUser() {
        currentUserFlow.emit(remoteDataSource.getCurrentUserInfo())
    }

    suspend fun updateUserPhoto(id: Long) {
        remoteDataSource.updateUserInfo(getCurrentUserOrFetch().copy(photo = PhotoDto(null, id)))
    }

    private suspend fun getCurrentUserOrFetch(): UserDto {
        val user = currentUserFlow.value ?: remoteDataSource.getCurrentUserInfo()
        if (currentUserFlow.value != user) {
            currentUserFlow.emit(user)
        }
        return user
    }

    suspend fun updateUserPhone(phone: String) {
        remoteDataSource.updateUserInfo(getCurrentUserOrFetch().copy(phone = phone))
//        remoteDataSource.getCurrentUserInfo()
    }

    suspend fun updateUserName(name: String, surname: String) {
        remoteDataSource.updateUserInfo(
            getCurrentUserOrFetch().copy(
                name = name,
                surname = surname
            )
        )
//        remoteDataSource.getCurrentUserInfo()
    }

    suspend fun getUserById(id: Long): Flow<User?> {
        return flowOf(userMapper.map(remoteDataSource.getUserById(id)))
    }

}