package ru.barteromeo.domain.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.RemoteDataSource
import ru.barteromeo.data.client.offer.WishlistClient
import ru.barteromeo.data.dto.SearchOfferDto
import ru.barteromeo.data.network.safeApiCall
import ru.barteromeo.domain.mapper.OfferMapper
import ru.barteromeo.domain.model.SearchOffer
import ru.barteromeo.util.data.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WishlistRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val wishlistClient: WishlistClient,
    private val offerMapper: OfferMapper,
) {

    suspend fun resolveOffers(): Flow<PagingData<SearchOffer>> {
        return resolveOffersFromNet().map { pagedData ->
            pagedData.map {
                offerMapper.mapOffer(it)
            }
        }
    }

    private suspend fun resolveOffersFromNet(): Flow<PagingData<SearchOfferDto>> {
        return Pager(
            config = PagingConfig(
                enablePlaceholders = false,
                pageSize = NETWORK_PAGE_SIZE
            ),
            pagingSourceFactory = {
                wishlistClient
            }
        ).flow
    }

    suspend fun addToWishlist(id: Long): Flow<Resource<*>> {
        return flow { emit(safeApiCall { remoteDataSource.addToWishlist(id) }) }
    }

    suspend fun removeFromWishlist(id: Long): Flow<Resource<*>> {
        return flow { emit(safeApiCall { remoteDataSource.removeFromWishlist(id) })}
    }


    companion object {
        private const val NETWORK_PAGE_SIZE = 4
    }

}