package ru.barteromeo.domain.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.client.offer.CategoryClient
import ru.barteromeo.data.client.offer.CategoryForUserClient
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.domain.mapper.CategoryMapper
import ru.barteromeo.domain.model.Category
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryRepository @Inject constructor(
    private val baseApi: BaseApi,
    private val categoryMapper: CategoryMapper,
    private val categoryClient: CategoryClient,
    private val categoryForUserClient: CategoryForUserClient,
) {

    suspend fun getAllCategories(): Flow<List<Category>> {
        return flow {
            emit(baseApi.getCategories(100, 1).categories.map { categoryMapper.map(it) })
        }
    }

    suspend fun getCategoriesForUser(): Flow<PagingData<Category>> {
        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = { categoryForUserClient }).flow.map {
            it.map { categoryMapper.map(it) }
        }
    }

}