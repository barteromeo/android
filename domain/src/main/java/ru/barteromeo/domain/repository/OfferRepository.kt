package ru.barteromeo.domain.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.RemoteDataSource
import ru.barteromeo.data.client.offer.OfferClient
import ru.barteromeo.data.dto.SearchOfferDto
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.domain.mapper.OfferMapper
import ru.barteromeo.domain.model.Offer
import ru.barteromeo.domain.model.OfferType
import ru.barteromeo.domain.model.SearchOffer
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OfferRepository @Inject constructor(
    private val baseApi: BaseApi,
    private val remoteDataSource: RemoteDataSource,
    private val offerMapper: OfferMapper,
) {


    suspend fun resolveOffers(request: String?): Flow<PagingData<SearchOffer>> {
        return resolveOffersFromNet(request).map { pagedData ->
            pagedData.map {
                offerMapper.mapOffer(it)
            }
        }
    }

    suspend fun resolveOfferById(id: Long): Flow<Offer> {
        return flow {
            emit(remoteDataSource.getOfferById(id))
        }
            .flowOn(Dispatchers.IO)
            .map {
                offerMapper.mapOffer(it)
            }
    }


    private suspend fun resolveOffersFromNet(request: String?): Flow<PagingData<SearchOfferDto>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = NETWORK_PAGE_SIZE),
            pagingSourceFactory = {
                OfferClient(baseApi, request)
            }
        ).flow
    }

    suspend fun createOffer(
        title: String,
        photoIds: List<Int>,
        description: String,
        changeOn: String,
        phone: String,
        whereToMeet: String,
        tags: List<String>,
        type: OfferType,
        categoryIds: List<Long>,
    ) {
        remoteDataSource.createOffer(
            title = title,
            photoIds = photoIds,
            description = description,
            changeOn = changeOn,
            phone = phone,
            whereToMeet = whereToMeet,
            tags = tags,
            type = type.name,
            categoryIds = categoryIds,
        )
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 2
    }
}