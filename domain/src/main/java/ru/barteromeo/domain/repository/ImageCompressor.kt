package ru.barteromeo.domain.repository

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.annotation.CheckResult
import androidx.annotation.WorkerThread
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy
import dagger.Reusable
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.ByteArrayOutputStream
import javax.inject.Inject

@Reusable
class ImageCompressor @Inject constructor(
    @ApplicationContext private val context: Context
) {

    private val requestManager by lazy { Glide.with(context) }

    @CheckResult
    @WorkerThread
    fun compressImage(imageUri: Uri): ByteArray? {
        val scaledBitmap = requestManager.asBitmap()
            .load(imageUri)
            .override(MAX_SIZE_PX)
            .downsample(DownsampleStrategy.CENTER_INSIDE)
            .skipMemoryCache(true).diskCacheStrategy(
                DiskCacheStrategy.NONE
            ).submit().get()

        val out = ByteArrayOutputStream()
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, COMPRESS_QUALITY_PERCENT, out)
        return out.toByteArray()
    }

    companion object {
        private const val MAX_SIZE_PX = 1000
        private const val COMPRESS_QUALITY_PERCENT = 90
    }
}