package ru.barteromeo.domain.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.RemoteDataSource
import ru.barteromeo.data.client.offer.MyOffersClient
import ru.barteromeo.data.client.offer.OfferClient
import ru.barteromeo.data.dto.SearchOfferDto
import ru.barteromeo.data.network.BaseApi
import ru.barteromeo.domain.mapper.OfferMapper
import ru.barteromeo.domain.model.Offer
import ru.barteromeo.domain.model.SearchOffer
import javax.inject.Inject

class MyOffersRepository @Inject constructor(
    private val offerMapper: OfferMapper,
    private val myOffersClient: MyOffersClient
) {


    suspend fun resolveOffers(): Flow<PagingData<SearchOffer>> {
        return resolveOffersFromNet().map { pagedData ->
            pagedData.map {
                offerMapper.mapOffer(it)
            }
        }
    }

    private suspend fun resolveOffersFromNet(): Flow<PagingData<SearchOfferDto>> {
        return Pager(
            config = PagingConfig(
                enablePlaceholders = false,
                pageSize = NETWORK_PAGE_SIZE
            ),
            pagingSourceFactory = {
                myOffersClient
            }
        ).flow
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 2
    }
}