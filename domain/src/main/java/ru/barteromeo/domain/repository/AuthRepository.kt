package ru.barteromeo.domain.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import ru.barteromeo.data.RemoteDataSource
import ru.barteromeo.data.UserPreferences
import ru.barteromeo.data.network.safeApiCall
import ru.barteromeo.util.data.Resource
import java.lang.NullPointerException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val userPreferences: UserPreferences,
) {

    suspend fun login(username: String, password: String): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.login(username, password)
            }.also {
                val authDto = it.getSuccessResourceIfPresent()
                userPreferences.updateTokens(authDto?.accessToken, authDto?.refreshToken)
            })
        }.map {
            when (it) {
                is Resource.Success -> true
                is Resource.Failure -> throw it.throwable ?: return@map false
                is Resource.Loading -> false
            }
        }
    }

    suspend fun passwordRecoveryRequest(email: String): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.passwordRecoveryRequest(email)
            })
        }.map {
            when (it) {
                is Resource.Success -> true
                is Resource.Failure -> throw it.throwable ?: return@map false
                is Resource.Loading -> false
            }
        }
    }

    suspend fun passwordRecoveryConfirmRequest(email: String, code: String): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.passwordRecoveryConfirmRequest(email, code)
            })
        }.map {
            when (it) {
                is Resource.Success -> it.value.tokenIsValid
                is Resource.Failure -> throw it.throwable ?: return@map false
                is Resource.Loading -> false
            }
        }
    }

    suspend fun passwordRecover(email: String, code: String, newPassword: String): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.passwordRecover(email, code, newPassword)
            })
        }.map {
            when (it) {
                is Resource.Success -> true
                is Resource.Failure -> throw it.throwable ?: return@map false
                is Resource.Loading -> false
            }
        }
    }

    suspend fun register(
        email: String,
        password: String,
        firstName: String,
        surname: String,
        code: String
    ): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.register(email, password, firstName, surname, code)
            }.also {
                val authDto = it.getSuccessResourceIfPresent()
                userPreferences.updateTokens(authDto?.accessToken, authDto?.refreshToken)
            })
        }.map {
            when (it) {
                is Resource.Success -> true
                is Resource.Failure -> throw it.throwable ?: return@map false
                is Resource.Loading -> false
            }
        }
    }

    suspend fun refreshToken(token: String): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.refreshToken(token)
            }.also {
                val authDto = it.getSuccessResourceIfPresent()
                userPreferences.updateTokens(authDto?.accessToken, authDto?.refreshToken)
            })
        }.map {
            it.mapToBoolean()
        }
    }

    suspend fun requestConfirmEmailCode(email: String) {
        remoteDataSource.requestEmailCode(email)
    }

    suspend fun sendConfirmEmailCode(email: String, code: String): Flow<Boolean> {
        return flow {
            emit(safeApiCall {
                remoteDataSource.confirmEmail(email, code).tokenIsValid
            })
        }.map {
            it.getSuccessResourceIfPresent()
                ?: throw NullPointerException("Answer is null @ sendConfirmEmailCode, AuthRepository")
        }
    }

}