package ru.barteromeo.util.extensions

fun Boolean.orThrow(throwable: Throwable): Boolean {
    if (this) {
        return this
    }
    throw throwable
}