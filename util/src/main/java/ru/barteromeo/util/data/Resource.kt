package ru.barteromeo.util.data

sealed class Resource<out T> {
    data class Success<out T>(val value: T) : Resource<T>()
    data class Failure(
        val isNetworkError: Boolean,
        val errorCode: Int?,
        val throwable: Throwable?
    ) : Resource<Nothing>()

    object Loading : Resource<Nothing>()

    fun getSuccessResourceIfPresent(): T? {
        if (this is Success) {
            return value
        }
        return null
    }

    inline fun <R> transform(transform: (T) -> (R?)): Resource<R?> {
        return when (this) {
            is Failure -> this
            is Loading -> this
            is Success -> {
                try {
                    Success(transform(this.value))
                } catch (t: Throwable) {
                    Failure(false, null, t)
                }
            }
        }
    }

    inline fun <R> transformNotNull(transform: (T) -> (R?)): Resource<R> {
        return when (this) {
            is Failure -> this
            is Loading -> this
            is Success -> {
                try {
                    Success(transform(this.value)!!)
                } catch (t: Throwable) {
                    Failure(false, null, t)
                }
            }
        }
    }

    fun mapToBoolean(): Boolean {
        return when (this) {
            is Success -> true
            is Failure -> throw this.throwable ?: return false
            is Loading -> false
        }
    }
}
