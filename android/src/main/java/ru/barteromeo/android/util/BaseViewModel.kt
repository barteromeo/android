package ru.barteromeo.android.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler

open class BaseViewModel : ViewModel() {

    protected val mutableErrorLiveData = MutableLiveData<Throwable>()
    val errorLiveData: LiveData<Throwable>
        get() = mutableErrorLiveData

    protected val defaultExceptionHandler =
        CoroutineExceptionHandler { _, throwable -> mutableErrorLiveData.postValue(throwable) }
}