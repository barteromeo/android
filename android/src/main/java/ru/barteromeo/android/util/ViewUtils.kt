package ru.barteromeo.android.util

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.chip.Chip
import ru.barteromeo.android.R

fun View.gone() {
    this.visibility = GONE
}

fun View.visible() {
    this.visibility = VISIBLE
}

fun inflateChip(
    layoutInflater: LayoutInflater,
    parent: ViewGroup,
    category: String
): Chip {
    return (
            layoutInflater.inflate(R.layout.category_chip_item, parent, false) as Chip
            ).apply {
            text = category
        }
}

fun TextView.setTextOrGone(text: String?) {
    if (text == null) {
        gone()
    } else {
        this.text = text
        visible()
    }
}