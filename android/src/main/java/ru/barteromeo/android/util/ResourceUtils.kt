package ru.barteromeo.android.util

import android.content.res.AssetManager
import android.content.res.Resources
import android.graphics.drawable.Icon
import androidx.annotation.ArrayRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.PluralsRes
import androidx.annotation.RawRes
import androidx.annotation.StringRes
import dagger.Reusable
import java.io.InputStream
import javax.inject.Inject

@Reusable
class ResourceUtils @Inject constructor(private val resources: Resources) {

    fun getString(@StringRes stringResourceId: Int): String {
        return resources.getString(stringResourceId)
    }

    fun getFormattedString(
        @StringRes stringResourceId: Int,
        vararg formatArguments: Any?
    ): String {
        return resources.getString(stringResourceId, *formatArguments)
    }

    fun getQuantityString(@PluralsRes quantityResourceId: Int, quantity: Int): String {
        return resources.getQuantityString(quantityResourceId, quantity, quantity)
    }

    fun getFormattedQuantityString(
        @PluralsRes quantityResourceId: Int,
        quantity: Int,
        vararg formatArguments: Any
    ): String {
        return resources.getQuantityString(quantityResourceId, quantity, *formatArguments)
    }

    fun getStringArray(@ArrayRes stringArrayResourceId: Int): Array<String> {
        return resources.getStringArray(stringArrayResourceId)
    }

    @ColorInt
    fun getColor(@ColorRes colorRes: Int): Int {
        return resources.getColor(colorRes)
    }

    fun getIcon(@DrawableRes drawableResourceId: Int): Icon {
        return Icon.createWithResource(
            resources.getResourcePackageName(drawableResourceId),
            drawableResourceId
        )
    }

    fun getInteger(id: Int): Int {
        return resources.getInteger(id)
    }

    fun getDimensionPixelSize(id: Int): Int {
        return resources.getDimensionPixelSize(id)
    }

    fun getRawResource(@RawRes id: Int): InputStream {
        return resources.openRawResource(id)
    }

    val assets: AssetManager
        get() = resources.assets
}