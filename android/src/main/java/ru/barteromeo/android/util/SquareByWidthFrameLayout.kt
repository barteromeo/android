package ru.barteromeo.android.util

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

open class SquareByWidthFrameLayout(context: Context, attrs: AttributeSet?) :
    FrameLayout(context, attrs) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
    }

}