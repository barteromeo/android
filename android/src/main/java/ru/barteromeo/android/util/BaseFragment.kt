package ru.barteromeo.android.util

import android.widget.Toast
import androidx.fragment.app.Fragment
import timber.log.Timber

open class BaseFragment : Fragment() {

    protected open fun showError(t: Throwable) {
        Timber.e(t)
        Toast.makeText(requireContext(), "Caught exception ${t.message}", Toast.LENGTH_LONG).show()
    }

}