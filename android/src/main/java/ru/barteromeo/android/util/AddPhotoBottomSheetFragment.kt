package ru.barteromeo.android.util

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.databinding.AddPhotoBottomSheetFragmentBinding
import ru.barteromeo.android.util.FragmentsHelper.Companion.findListeners

@AndroidEntryPoint
class AddPhotoBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var binding: AddPhotoBottomSheetFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AddPhotoBottomSheetFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.addFromGalleryButton.setOnClickListener {
            onSelectFromGallery()
        }
        binding.takePhotoButton.setOnClickListener {
            onTakePhoto()
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        setTargetFragment(parentFragment, 0)
        super.show(manager, tag)
    }

    private fun onTakePhoto() {
        val listeners = findListeners(this, Listener::class.java)
        listeners.forEach {
            it.onTakeFromCamera()
        }
        dismiss()
    }

    private fun onSelectFromGallery() {
        val listeners = findListeners(this, Listener::class.java)
        listeners.forEach { it.onOpenGallery() }
        dismiss()
    }

    interface Listener {
        fun onTakeFromCamera()

        fun onOpenGallery()
    }

    companion object {
        fun newInstance() = AddPhotoBottomSheetFragment()

        const val TAG = "AddPhotoTag"
    }

}