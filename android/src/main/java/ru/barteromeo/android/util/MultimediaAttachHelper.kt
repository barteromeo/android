package ru.barteromeo.android.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import dagger.Reusable
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class MultimediaAttachHelper @Inject constructor() {

    private var cameraVideoPermissionLauncher: ActivityResultLauncher<String>? = null
    private var cameraPhotoPermissionLauncher: ActivityResultLauncher<String>? = null
    private var openMultipleDocumentLauncher: ActivityResultLauncher<Array<String>>? = null
    private var openDocumentLauncher: ActivityResultLauncher<Array<String>>? = null
    private var takePhotoLauncher: ActivityResultLauncher<Uri>? = null
    private var takeVideoLauncher: ActivityResultLauncher<Any>? = null

    fun registerCameraVideoPermissionContract(fragment: Fragment, callback: (Boolean) -> Unit) {
        cameraVideoPermissionLauncher =
            fragment.registerForActivityResult(
                ActivityResultContracts.RequestPermission(),
                callback
            )
    }

    fun registerCameraPhotoPermissionContract(fragment: Fragment, callback: (Boolean) -> Unit) {
        cameraPhotoPermissionLauncher =
            fragment.registerForActivityResult(
                ActivityResultContracts.RequestPermission(),
                callback
            )
    }

    fun launchCameraPermissonContract(type: MultimediaContentType) {
        when (type) {
            MultimediaContentType.VIDEO ->
                cameraVideoPermissionLauncher?.launch(android.Manifest.permission.CAMERA)
                    ?: Timber.wtf(CAMERA_PERMISSION_WTF_MESSAGE)
            MultimediaContentType.IMAGE ->
                cameraPhotoPermissionLauncher?.launch(android.Manifest.permission.CAMERA)
                    ?: Timber.wtf(CAMERA_PERMISSION_WTF_MESSAGE)
        }
    }

    fun registerOpenMultipleDocumentContract(fragment: Fragment, callback: (List<Uri>) -> Unit) {
        openMultipleDocumentLauncher =
            fragment.registerForActivityResult(
                ActivityResultContracts.OpenMultipleDocuments(),
                callback
            )
    }

    fun launchOpenMultipleDocumentContract(type: Type) {
        openMultipleDocumentLauncher?.launch(arrayOf(type.mimeType))
            ?: Timber.wtf("call registerOpenMultipleDocumentContract before fragment creation")
    }

    fun registerOpenDocumentContract(fragment: Fragment, callback: (Uri?) -> Unit) {
        openDocumentLauncher =
            fragment.registerForActivityResult(ActivityResultContracts.OpenDocument(), callback)
    }

    fun launchOpenDocumentContract(type: Type) {
        openDocumentLauncher?.launch(arrayOf(type.mimeType))
            ?: Timber.wtf("call registerOpenDocumentContract before fragment creation")
    }

    fun registerTakePhotoContract(fragment: Fragment, callback: (Uri?) -> Unit) {
        takePhotoLauncher =
            fragment.registerForActivityResult(ActivityResultContracts.TakePicture()) { photoLoaded ->
                val uri = if (photoLoaded) {
                    fragment.arguments?.getParcelable<Uri>(FRAGMENT_ARG_USER_PHOTO_URI)
                } else {
                    null
                }
                callback(uri)
            }
    }

    fun launchTakePhotoContract(fragment: Fragment) {
        fragment.context?.let { context ->
            takePhotoLauncher?.let {
                val uri = generatePhotoUri(context)
                if (fragment.arguments == null) {
                    fragment.arguments = bundleOf()
                }
                fragment.arguments?.putParcelable(FRAGMENT_ARG_USER_PHOTO_URI, uri)
                it.launch(uri)
            } ?: Timber.wtf("call registerTakePhotoContract before fragment creation")
        }
    }

    fun registerTakeVideoContract(fragment: Fragment, callback: (Uri?) -> Unit) {
        takeVideoLauncher = fragment.registerForActivityResult(TakeVideoUriContract(), callback)
    }

    fun launchTakeVideoContract() {
        takeVideoLauncher?.launch(Unit)
    }

    fun clear() {
        cameraVideoPermissionLauncher?.unregister()
        openMultipleDocumentLauncher?.unregister()
        openDocumentLauncher?.unregister()
        takeVideoLauncher?.unregister()
        takePhotoLauncher?.unregister()
        cameraPhotoPermissionLauncher?.unregister()
        cameraVideoPermissionLauncher = null
        cameraPhotoPermissionLauncher = null
        openMultipleDocumentLauncher = null
        openDocumentLauncher = null
        takeVideoLauncher = null
        takePhotoLauncher = null
    }


    private fun generatePhotoUri(context: Context): Uri {
        val directory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val timestamp = System.currentTimeMillis()
        val file = File.createTempFile(timestamp.toString(), ".jpg", directory)
        return FileProvider.getUriForFile(context, "ru.barteromeo.android.provider", file)
    }

    enum class Type(val mimeType: String) {
        IMAGE("image/*"), VIDEO("video/*")
    }

    private class TakeVideoUriContract : ActivityResultContract<Any, Uri?>() {
        override fun createIntent(context: Context, input: Any?): Intent {
            return Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        }

        override fun parseResult(resultCode: Int, intent: Intent?): Uri? {
            return if (resultCode == Activity.RESULT_OK && intent != null) {
                intent.data
            } else {
                null
            }
        }
    }

    enum class MultimediaContentType {
        IMAGE,
        VIDEO
    }

    companion object {
        private const val FRAGMENT_ARG_USER_PHOTO_URI = "camera_request_user_photo_uri"
        private const val CAMERA_PERMISSION_WTF_MESSAGE =
            "call registerCameraPermissionContract before fragment creation"
    }
}