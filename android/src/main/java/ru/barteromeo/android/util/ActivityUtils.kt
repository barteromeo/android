package ru.barteromeo.android.util

import android.app.Activity
import ru.barteromeo.android.ui.start.MainActivity

fun Activity.hideBottomNav() {
    (this as? MainActivity)?.hideBottomNav()
}

fun Activity.showBottomNav() {
    (this as? MainActivity)?.showBottomNav()
}