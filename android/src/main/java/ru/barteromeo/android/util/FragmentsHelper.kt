package ru.barteromeo.android.util

import android.app.Activity
import androidx.fragment.app.Fragment
import java.util.stream.Stream

class FragmentsHelper {


    companion object {
        fun <T> findListeners(
            fragment: Fragment,
            listenerClass: Class<T>
        ): List<T> {
            val fragmentParents: MutableList<Any> = ArrayList()
            var parentFragment = fragment.parentFragment
            while (parentFragment != null) {
                fragmentParents.add(parentFragment)
                parentFragment = parentFragment.parentFragment
            }
            val targetFragment = fragment.targetFragment
            if (targetFragment != null) {
                fragmentParents.add(targetFragment)
            }
            val parentActivity: Activity? = fragment.activity
            if (parentActivity != null) {
                fragmentParents.add(parentActivity)
            }
            return fragmentParents.filter { listenerClass.isInstance(it) }
                .map { it as T }
        }
    }
}