package ru.barteromeo.android.util

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.databinding.InputTextItemBinding

class InputTextRecyclerItem<T>(
    private val hint: String,
    private val initialValue: String? = null,
    private val producer: (String?) -> T,
) : AbstractBindingItem<InputTextItemBinding>() {

    private var editText: EditText? = null

    fun produce(): T {
        return producer(editText?.text?.toString())
    }

    override fun bindView(binding: InputTextItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.container.hint = hint
        editText = binding.editText
        editText?.setText(initialValue.orEmpty(), TextView.BufferType.EDITABLE)
    }

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): InputTextItemBinding {
        return InputTextItemBinding.inflate(inflater, parent, false)
    }

    override fun unbindView(binding: InputTextItemBinding) {
        super.unbindView(binding)
        editText = null
    }

    override val type: Int
        get() = -1
}