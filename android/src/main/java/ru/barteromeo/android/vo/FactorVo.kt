package ru.barteromeo.android.vo

data class FactorVo(
    val title: String,
    val content: String
)