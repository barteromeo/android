package ru.barteromeo.android.vo

import ru.barteromeo.domain.model.OfferStatus
import ru.barteromeo.domain.model.OfferType
import java.util.*

data class OfferVo(
    val id: Long,
    val title: String,
    val description: String,
    val photos: List<String>,
    val views: Int,
    val offerStatus: OfferStatus,
    val offerType: OfferType,
    val categories: List<CategoryVo>,
    val userPhoto: String?,
    val userName: String,
    val updated: Date,
    val address: String,
    val factors: List<FactorVo>,
    val phone: String?,
    val authorId: Long,
)