package ru.barteromeo.android.vo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryVo(
    val id: Long,
    val value: String,
    val photo: String?,
) : Parcelable