package ru.barteromeo.android.ui.onboarding.password.recovery.email

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PasswordRecoveryEmailViewModel @Inject constructor(
    private val passwordRecoveryRequestUseCase: PasswordRecoveryRequestUseCase
) : BaseViewModel() {

    val recoveryEmailLiveData: LiveData<Boolean>
        get() = recoveryEmailMutableLiveData

    private val recoveryEmailMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun onPasswordRecoverClick(
        email: String,
    ) {
        viewModelScope.launch {
            passwordRecoveryRequestUseCase.request(email).catch {
                mutableErrorLiveData.postValue(it)
            }.onEach {
                recoveryEmailMutableLiveData.postValue(it)
            }
                .launchIn(this)
        }
    }
}