package ru.barteromeo.android.ui.createoffer.category

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Category
import ru.barteromeo.domain.usecase.category.GetCategoriesUseCase
import javax.inject.Inject

@Reusable
class CreateOfferCategoriesUseCases @Inject constructor(
    private val getCategoriesUseCase: Lazy<GetCategoriesUseCase>,
) {

    suspend fun resolveCategories(): Flow<List<Category>> {
        return getCategoriesUseCase.get().execute()
    }
}