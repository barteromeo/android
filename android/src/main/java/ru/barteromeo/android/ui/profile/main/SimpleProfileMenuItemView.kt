package ru.barteromeo.android.ui.profile.main

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import ru.barteromeo.android.databinding.SimpleProfileMenuItemViewBinding
import ru.barteromeo.android.util.setTextOrGone

class SimpleProfileMenuItemView @JvmOverloads constructor(
    context: Context,
    attr: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(
    context,
    attr,
    defStyleAttr
) {

    private var binding: SimpleProfileMenuItemViewBinding =
        SimpleProfileMenuItemViewBinding.inflate(LayoutInflater.from(context))

    init {
        addView(binding.root)
    }

    fun show(title: String) {
        binding.profileMenuItemTitle.setTextOrGone(title)
    }

}