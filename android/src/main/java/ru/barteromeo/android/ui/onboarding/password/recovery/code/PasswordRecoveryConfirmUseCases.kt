package ru.barteromeo.android.ui.onboarding.password.recovery.code

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.usecase.auth.PasswordRecoveryConfirmUseCase
import ru.barteromeo.domain.usecase.auth.PasswordRecoveryRequestUseCase
import ru.barteromeo.domain.usecase.auth.SendConfirmRequestUseCase
import javax.inject.Inject

@Reusable
class PasswordRecoveryConfirmUseCases @Inject constructor(
    private val passwordRecoveryConfirmUseCase: Lazy<PasswordRecoveryConfirmUseCase>,
    private val passwordRecoveryRequestUseCase: Lazy<PasswordRecoveryRequestUseCase>
) {

    suspend fun confirm(
        email: String,
        code: String
    ): Flow<Boolean> {
        return passwordRecoveryConfirmUseCase.get().execute(email, code)
    }

    suspend fun request(
        email: String
    ): Flow<Boolean> {
        return passwordRecoveryRequestUseCase.get().execute(email)
    }

}