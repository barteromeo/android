package ru.barteromeo.android.ui.profile.main

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.ui.createoffer.details.CreateOfferDetailsViewModel
import ru.barteromeo.android.ui.createoffer.details.PhotoVo
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.domain.model.User
import javax.inject.Inject

@HiltViewModel
class ProfileMainViewModel @Inject constructor(
    private val profileMainUseCases: ProfileMainUseCases
) : BaseViewModel() {

    val userLiveData: LiveData<User>
        get() = userMutableLiveData

    private val userMutableLiveData: MutableLiveData<User> = MutableLiveData()

    fun fetchUserInfo() {
        viewModelScope.launch {
            profileMainUseCases.getCurrentUser().onEach {
                if (it != null) userMutableLiveData.postValue(it)
            }.catch {
                mutableErrorLiveData.postValue(it)
            }.launchIn(this)
        }
    }

    fun uploadPhoto(uri: Uri) {
        viewModelScope.launch {
            profileMainUseCases.uploadPhoto(uri)
                .onEach {
                    this.launch(defaultExceptionHandler) {
                        profileMainUseCases.updateProfilePhoto(it.id.toLong())
                    }.invokeOnCompletion {
                        if (it != null)
                            mutableErrorLiveData.postValue(it)
                        else
                            fetchUserInfo()
                    }
                }
                .catch {
                    mutableErrorLiveData.postValue(it)
                }
                .launchIn(this)
        }
    }

}