package ru.barteromeo.android.ui.onboarding.register

import dagger.Lazy
import dagger.Reusable
import ru.barteromeo.domain.usecase.auth.SendConfirmRequestUseCase
import javax.inject.Inject

@Reusable
class RegisterUseCases @Inject constructor(
    private val sendConfirmRequestUseCase: Lazy<SendConfirmRequestUseCase>,
) {

    suspend fun sendConfirmRequest(
        email: String,
    ) {
        return sendConfirmRequestUseCase.get().execute(email)
    }

}