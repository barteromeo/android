package ru.barteromeo.android.ui.offer

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import ru.barteromeo.android.databinding.OfferFactorViewBinding
import ru.barteromeo.android.vo.FactorVo

class SimpleFactorView @JvmOverloads constructor(
    context: Context,
    attr: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : FrameLayout(
    context,
    attr,
    defStyleAttr
) {

    private var binding: OfferFactorViewBinding = OfferFactorViewBinding.inflate(LayoutInflater.from(context))

    init {
        addView(binding.root)
    }


    fun showVo(vo: FactorVo) {
        binding.titleTextView.text = vo.title
        binding.contentTextView.text = vo.content
    }

}