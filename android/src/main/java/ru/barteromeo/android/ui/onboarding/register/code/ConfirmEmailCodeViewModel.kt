package ru.barteromeo.android.ui.onboarding.register.code

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class ConfirmEmailCodeViewModel @Inject constructor(
    private val confirmEmailCodeUseCases: ConfirmEmailCodeUseCases
) : BaseViewModel() {
    val isLoggedInLiveData: LiveData<Boolean>
        get() = isLoggedInMutableLiveData

    private val isLoggedInMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun onRegister(
        email: String,
        name: String,
        surname: String,
        password: String,
        code: String,
    ) {
        viewModelScope.launch {
            confirmEmailCodeUseCases.confirm(code, email).catch {
                mutableErrorLiveData.postValue(it)
            }.onEach {
                onCodeConfirmed(it, email, name, surname, password, code)
            }
                .launchIn(this)
        }
    }

    private fun onCodeConfirmed(
        isValid: Boolean,
        email: String,
        name: String,
        surname: String,
        password: String,
        code: String,
    ) {
        viewModelScope.launch {
            if (isValid) {
                confirmEmailCodeUseCases.register(email, name, surname, password, code).onEach {
                    isLoggedInMutableLiveData.postValue(isValid)
                }.catch {
                    mutableErrorLiveData.postValue(it)
                }.launchIn(this)
            } else {
                mutableErrorLiveData.postValue(Exception("Code not valid"))
            }
        }
    }
}