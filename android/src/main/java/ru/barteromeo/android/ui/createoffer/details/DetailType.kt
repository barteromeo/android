package ru.barteromeo.android.ui.createoffer.details

sealed class DetailType(
    val value: String,
) {

    class Title(value: String) : DetailType(value)
    class Description(value: String) : DetailType(value)
    class ExchangeFor(value: String) : DetailType(value)
    class WhereToMeet(value: String) : DetailType(value)
    class Phone(value: String) : DetailType(value)
    object Empty : DetailType("Empty")

}