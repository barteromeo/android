package ru.barteromeo.android.ui.onboarding.register.code

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.ConfirmEmailCodeFragmentBinding
import ru.barteromeo.android.util.BaseFragment

@AndroidEntryPoint
class ConfirmEmailCodeFragment : BaseFragment() {

    companion object {
        fun newInstance() = ConfirmEmailCodeFragment()
    }

    private val viewModel: ConfirmEmailCodeViewModel by viewModels()
    private lateinit var binding: ConfirmEmailCodeFragmentBinding
    private var code = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ConfirmEmailCodeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun sendRecoverCode(
        emailText: String,
        surname: String,
        name: String,
        password: String,
    ) {
        val codeText = binding.emailCodeEditText.text.toString()
        code = codeText
        viewModel.onRegister(
            email = emailText,
            name = name,
            surname = surname,
            password = password,
            code = codeText
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val emailText = arguments?.getString("email", "") ?: ""
        val surnameText = arguments?.getString("surname", "") ?: ""
        val nameText = arguments?.getString("name", "") ?: ""
        val passwordText = arguments?.getString("password", "") ?: ""
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        binding.emailCodeSendButton.setOnClickListener {
            sendRecoverCode(emailText, surnameText, nameText, passwordText)
        }
        binding.emailCodeEditText.addTextChangedListener {
            binding.emailCodeTextInputLayout.error = null
        }
        viewModel.isLoggedInLiveData.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(
                    R.id.action_navigate_to_tabs
                )
            }
        }
    }

}