package ru.barteromeo.android.ui.onboarding.password.enter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class EnterPasswordViewModel @Inject constructor(
    private val useCases: LoginUseCases
) : BaseViewModel() {

    val loggedInLiveData: LiveData<Boolean>
        get() = loggedInMutableLiveData

    private val loggedInMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun login(username: String, password: String) {
        viewModelScope.launch {
            useCases.login(username, password).onEach {
                loggedInMutableLiveData.postValue(it)
            }
                .catch {
                    mutableErrorLiveData.postValue(it)
                }.launchIn(this)
        }
    }

}