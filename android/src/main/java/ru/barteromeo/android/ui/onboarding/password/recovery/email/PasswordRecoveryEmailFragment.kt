package ru.barteromeo.android.ui.onboarding.password.recovery.email

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.PasswordRecoveryEmailFragmentBinding
import ru.barteromeo.android.ui.onboarding.register.RegisterFragment
import ru.barteromeo.android.util.BaseFragment
import java.util.regex.Pattern

@AndroidEntryPoint
class PasswordRecoveryEmailFragment : BaseFragment() {

    companion object {
        fun newInstance() = PasswordRecoveryEmailFragment()
        private val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" + "(edu\\.hse\\.ru|hse\\.ru)"
        )
    }

    private val viewModel: PasswordRecoveryEmailViewModel by viewModels()
    private lateinit var binding: PasswordRecoveryEmailFragmentBinding
    private var globalEmailText: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PasswordRecoveryEmailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.passwordRecoveryEmailNextButton.setOnClickListener {
            val emailText = binding.passwordRecoveryEmailEditText.text.toString()
            globalEmailText = emailText
            if (EMAIL_ADDRESS_PATTERN.matcher(emailText).matches()) {
                viewModel.onPasswordRecoverClick(emailText)
            } else {
                binding.passwordRecoveryEmailTextInputLayout.error = getString(R.string.email_error)
            }
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        binding.passwordRecoveryEmailEditText.addTextChangedListener {
            binding.passwordRecoveryEmailTextInputLayout.error = null
        }
        viewModel.recoveryEmailLiveData.observe(viewLifecycleOwner) {
            if (it) {
                val bundle = bundleOf("email" to globalEmailText)
                findNavController().navigate(
                    R.id.action_password_recovery_email_fragment_to_password_recovery_code_fragment,
                    bundle
                )
            }
        }
    }

}