package ru.barteromeo.android.ui.profile.name

import javax.inject.Inject
import dagger.Lazy
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.User
import ru.barteromeo.domain.usecase.user.GetCurrentUserUseCase
import ru.barteromeo.domain.usecase.user.UpdateNameUseCase

class ChangeNameUseCases @Inject constructor(
    private val changeNameUseCase: Lazy<UpdateNameUseCase>,
    private val fetchUserUseCase: Lazy<GetCurrentUserUseCase>,
) {

    suspend fun changeName(name: String, surname: String) {
        changeNameUseCase.get().execute(name, surname)
    }

    suspend fun fetchUser(): Flow<User?> {
        return fetchUserUseCase.get().execute()
    }

}