package ru.barteromeo.android.ui.onboarding.choosecity

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R

@AndroidEntryPoint
class ChooseCityFragment : Fragment() {

    companion object {
        fun newInstance() = ChooseCityFragment()
    }

    private val viewModel: ChooseCityViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.choose_city_fragment, container, false)
    }

}