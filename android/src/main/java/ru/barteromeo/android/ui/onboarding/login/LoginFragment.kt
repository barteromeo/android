package ru.barteromeo.android.ui.onboarding.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.LoginFragmentBinding
import java.util.regex.Pattern

@AndroidEntryPoint
class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
        private val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" + "(edu\\.hse\\.ru|hse\\.ru)"
        )
    }

    private lateinit var binding: LoginFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loginNextButton.setOnClickListener {
            val text = binding.loginEditText.text.toString()
            if (EMAIL_ADDRESS_PATTERN.matcher(text).matches()) {
                val bundle = bundleOf("email" to text)
                findNavController().navigate(
                    R.id.action_login_fragment_to_enter_password_fragment,
                    bundle
                )
            } else {
                binding.loginTextInputLayout.error = getString(R.string.email_error)
            }
        }
        binding.loginEditText.addTextChangedListener {
            binding.loginTextInputLayout.error = null
        }
    }

}