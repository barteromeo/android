package ru.barteromeo.android.ui.onboarding.password.recovery.success

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.PasswordRecoverySuccessFragmentBinding
import ru.barteromeo.android.util.BaseFragment

@AndroidEntryPoint
class PasswordRecoverySuccessFragment : BaseFragment() {

    companion object {
        fun newInstance() = PasswordRecoverySuccessFragment()
    }

    private val viewModel: PasswordRecoverySuccessViewModel by viewModels()
    private lateinit var binding: PasswordRecoverySuccessFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PasswordRecoverySuccessFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        binding.recoveryLoginButton.setOnClickListener {
            findNavController().navigate(
                R.id.action_recovery_to_login_flow,
            )
        }
    }

}