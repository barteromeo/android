package ru.barteromeo.android.ui.onboarding.password.recovery.newpassword

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.usecase.auth.PasswordRecoveryUseCase
import javax.inject.Inject

@Reusable
class PasswordRecoveryUseCase @Inject constructor(
    private val passwordRecoveryUseCase: Lazy<PasswordRecoveryUseCase>
) {

    suspend fun recovery(
        email: String,
        code: String,
        newPassword: String
    ): Flow<Boolean> {
        return passwordRecoveryUseCase.get().execute(email, code, newPassword)
    }

}