package ru.barteromeo.android.ui.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.StartFragmentBinding
import ru.barteromeo.android.util.BaseFragment

@AndroidEntryPoint
class StartFragment : BaseFragment() {

    companion object {
        fun newInstance() = StartFragment()
    }

    private val startViewModel: StartViewModel by viewModels()
    private lateinit var binding: StartFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startViewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        startViewModel.startNavCommand.observe(viewLifecycleOwner) { startNavCommand ->
            val navController = Navigation.findNavController(
                requireActivity(),
                R.id.activity_main_nav_host
            )

            val mainGraph = navController.navInflater.inflate(R.navigation.mobile_navigation)

            // Way to change first screen at runtime.
            mainGraph.startDestination = when (startNavCommand) {
                StartNavCommand.NAVIGATE_TO_TABS -> R.id.navigation_tabs
                StartNavCommand.NAVIGATE_TO_AUTH -> R.id.auth_flow_navigation
                null -> throw IllegalArgumentException("Illegal start navigation command")
            }

            navController.graph = mainGraph
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = StartFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

}