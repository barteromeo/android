package ru.barteromeo.android.ui.createoffer.details

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.CreateOfferDetailsFragmentBinding
import ru.barteromeo.android.util.AddPhotoBottomSheetFragment
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.InputTextRecyclerItem
import ru.barteromeo.android.util.MultimediaAttachHelper
import ru.barteromeo.domain.model.OfferType
import javax.inject.Inject

@AndroidEntryPoint
class CreateOfferDetailsFragment : BaseFragment(), AddPhotoBottomSheetFragment.Listener {

    companion object {
        fun newInstance() = CreateOfferDetailsFragment()
        private const val MAX_PHOTOS = 10
    }

    private val viewModel: CreateOfferDetailsViewModel by activityViewModels()
    private lateinit var binding: CreateOfferDetailsFragmentBinding
    private val photoAdapter = FastItemAdapter<IItem<*>>().apply {
        setHasStableIds(false)
    }
    private val detailAdapter = FastItemAdapter<InputTextRecyclerItem<DetailType>>().apply {
        setHasStableIds(false)
    }

    @Inject
    lateinit var multimediaAttachHelper: MultimediaAttachHelper

    @Inject
    lateinit var requestManager: RequestManager

    override fun onAttach(context: Context) {
        super.onAttach(context)
        registerActivityResultContracts()
    }

    override fun onDetach() {
        super.onDetach()
        multimediaAttachHelper.clear()
        viewModel.clearState()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CreateOfferDetailsFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.photoRecyclerView) {
            layoutManager = GridLayoutManager(
                context,
                4,
            )
            adapter = photoAdapter
        }
        setUpDetails()
        with(binding.offerDetailRecyclerView) {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = detailAdapter
        }
        viewModel.photosLiveData.observe(viewLifecycleOwner) {
            setPhotos(it, requestManager)
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
    }

    private fun onRemovePhoto(photoVo: PhotoVo) {

    }

    private fun onReloadPhoto(photoVo: PhotoVo.Local) {

    }

    private fun onAddPhoto() {
        AddPhotoBottomSheetFragment.newInstance()
            .show(childFragmentManager, AddPhotoBottomSheetFragment.TAG)
    }

    private fun setUpDetails() {
        val detailItems = mutableListOf<InputTextRecyclerItem<DetailType>>()
        with(detailItems) {
            add(
                InputTextRecyclerItem("Название товара") {
                    if (it == null) DetailType.Empty else DetailType.Title(it)
                }
            )
            add(
                InputTextRecyclerItem("Описание") {
                    if (it == null) DetailType.Empty else DetailType.Description(it)
                }
            )
            add(
                InputTextRecyclerItem("Меняю на") {
                    if (it == null) DetailType.Empty else DetailType.ExchangeFor(it)
                }
            )
            add(
                InputTextRecyclerItem("Где встретиться") {
                    if (it == null) DetailType.Empty else DetailType.WhereToMeet(it)
                }
            )
            add(
                InputTextRecyclerItem("Телефон") {
                    if (it == null) DetailType.Empty else DetailType.Phone(it)
                }
            )
        }
        detailAdapter.itemAdapter.set(detailItems)
        binding.createOfferButton.setOnClickListener {
            viewModel.createOffer(
                details = detailItems.map { it.produce() },
                tags = arguments?.getParcelableArrayList("categories") ?: emptyList(),
                type = (arguments?.get("type") as? OfferType) ?: OfferType.GOOD,
                callback = this::navigateToMain
            )
        }
    }

    private fun navigateToMain() {
        findNavController().popBackStack(R.id.create_offer_type_navigation, false)
    }

    private fun setPhotos(photoVoList: List<PhotoVo>, requestManager: RequestManager) {
        val photoListSize = photoVoList.size
        val uploadedPhotoAdapterItems: List<IItem<*>> = photoVoList.map {
            UploadPhotoItem(
                requestManager, it,
                this::onRemovePhoto,
                this::onReloadPhoto
            )
        }
        val totalAdapterItems = if (photoListSize >= MAX_PHOTOS) {
            uploadedPhotoAdapterItems
        } else {
            uploadedPhotoAdapterItems + SelectPhotoAdapterItem(this::onAddPhoto)
        }
        photoAdapter.itemAdapter.clear()
        photoAdapter.itemAdapter.set(totalAdapterItems)
    }

    override fun onTakeFromCamera() {

        context?.let {
            val isAllowed =
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            if (isAllowed) {
                multimediaAttachHelper.launchTakePhotoContract(this)
            } else {
                multimediaAttachHelper.launchCameraPermissonContract(MultimediaAttachHelper.MultimediaContentType.IMAGE)
            }
        }
    }

    private fun registerActivityResultContracts() {
        with(multimediaAttachHelper) {
            registerOpenMultipleDocumentContract(this@CreateOfferDetailsFragment) { uris ->
                if (!uris.isNullOrEmpty()) {
                    viewModel.uploadPhotos(uris)
                }
            }
            registerTakePhotoContract(this@CreateOfferDetailsFragment) { photoUri ->
                if (photoUri != null) {
                    viewModel.uploadPhotos(listOf(photoUri))
                }
            }
            registerCameraPhotoPermissionContract(this@CreateOfferDetailsFragment) { isAllowed ->
                if (isAllowed) {
                    requestPhotoFromCamera()
                }
            }
        }
    }

    private fun requestPhotoFromCamera() {
        multimediaAttachHelper.launchTakePhotoContract(this)
    }

    override fun onOpenGallery() {
        multimediaAttachHelper.launchOpenMultipleDocumentContract(MultimediaAttachHelper.Type.IMAGE)
    }

}