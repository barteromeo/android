package ru.barteromeo.android.ui.onboarding.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.RegisterFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import java.util.regex.Pattern

@AndroidEntryPoint
class RegisterFragment : BaseFragment() {

    companion object {
        fun newInstance() = RegisterFragment()
        private val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" + "(edu\\.hse\\.ru|hse\\.ru)"
        )
    }

    private val viewModel: RegisterViewModel by viewModels()
    private lateinit var binding: RegisterFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = RegisterFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loginButton.setOnClickListener {
            findNavController().navigate(R.id.action_register_fragment_to_login_flow)
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        binding.registerButton.setOnClickListener {
            val nameText = binding.firstnameEditText.text.toString()
            val surnameText = binding.surnameEditText.text.toString()
            val passwordText = binding.passwordEditText.text.toString()
            val emailText = binding.emailEditText.text.toString()
            if (nameText.isBlank()) {
                binding.firstnameTextInputLayout.error = getString(R.string.field_is_empty)
            }
            if (surnameText.isBlank()) {
                binding.surnameEditText.error = getString(R.string.field_is_empty)
            }
            if (passwordText.isBlank()) {
                binding.passwordEditText.error = getString(R.string.field_is_empty)
            }

            if (EMAIL_ADDRESS_PATTERN.matcher(emailText).matches()) {
                viewModel.onRegisterClick(emailText)
            } else {
                binding.emailTextInputLayout.error = getString(R.string.email_error)
            }
        }
        binding.emailEditText.addTextChangedListener {
            binding.emailTextInputLayout.error = null
        }
        binding.passwordEditText.addTextChangedListener {
            binding.passwordEditText.error = null
        }
        binding.surnameEditText.addTextChangedListener {
            binding.surnameEditText.error = null
        }
        binding.firstnameEditText.addTextChangedListener {
            binding.firstnameEditText.error = null
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.loggedInLiveData.observe(viewLifecycleOwner) {
            if (it) {
                val nameText = binding.firstnameEditText.text.toString()
                val surnameText = binding.surnameEditText.text.toString()
                val passwordText = binding.passwordEditText.text.toString()
                val emailText = binding.emailEditText.text.toString()
                findNavController().navigate(
                    R.id.action_register_fragment_to_confirm_email_fragment,
                    bundleOf(
                        "name" to nameText,
                        "surname" to surnameText,
                        "password" to passwordText,
                        "email" to emailText,
                    )
                )
            }
        }
    }

}