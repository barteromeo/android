package ru.barteromeo.android.ui.offer

import dagger.Reusable
import ru.barteromeo.android.R
import ru.barteromeo.android.util.ResourceUtils
import ru.barteromeo.android.vo.FactorVo
import ru.barteromeo.android.vo.OfferVo
import ru.barteromeo.domain.model.Offer
import javax.inject.Inject

@Reusable
class OfferFormatter @Inject constructor(
    private val resourceUtils: ResourceUtils,
    private val categoryFormatter: CategoryFormatter
) {

    fun format(offer: Offer): OfferVo {
        return OfferVo(

            id = offer.id,
            title = offer.title,
            description = offer.description,
            photos = offer.photos,
            views = offer.views,
            offerStatus = offer.offerStatus,
            offerType = offer.offerType,
            categories = offer.categories.map { category -> categoryFormatter.format(category) },
            userPhoto = offer.user.photo,
            userName = "${offer.user.name}\u00a0${offer.user.surname}",
            updated = offer.updated,
            address = offer.location.address,
            factors = formatFactors(offer),
            phone = offer.phone,
            authorId = offer.user.id,
        )
    }

    private fun formatFactors(offer: Offer): List<FactorVo> {
        return listOf(
            FactorVo(resourceUtils.getString(R.string.phone), offer.phone),
            FactorVo(resourceUtils.getString(R.string.metro), offer.location.metro),
            FactorVo(resourceUtils.getString(R.string.city), offer.location.city),
            FactorVo("Адрес:", offer.location.address),
            FactorVo("Меняю на:", offer.changeOn)
        )
    }

}