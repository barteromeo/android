package ru.barteromeo.android.ui.profile.main

import android.net.Uri
import ru.barteromeo.domain.usecase.user.GetCurrentUserUseCase
import javax.inject.Inject
import dagger.Lazy
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.data.dto.UploadResponseDto
import ru.barteromeo.domain.model.User
import ru.barteromeo.domain.usecase.resource.UploadResourceUseCase
import ru.barteromeo.domain.usecase.user.UpdateAvatarUseCase

class ProfileMainUseCases @Inject constructor(
    private val getCurrentUserUseCase: Lazy<GetCurrentUserUseCase>,
    private val uploadResourceUseCase: Lazy<UploadResourceUseCase>,
    private val updateAvatarUseCase: Lazy<UpdateAvatarUseCase>,
) {

    suspend fun getCurrentUser(): Flow<User?> {
        return getCurrentUserUseCase.get().execute(true)
    }

    suspend fun uploadPhoto(uri: Uri): Flow<UploadResponseDto> {
        return uploadResourceUseCase.get().execute(uri)
    }

    suspend fun updateProfilePhoto(id: Long) {
        updateAvatarUseCase.get().execute(id)
    }

}