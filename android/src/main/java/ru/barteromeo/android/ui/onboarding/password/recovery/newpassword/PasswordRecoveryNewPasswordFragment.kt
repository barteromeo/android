package ru.barteromeo.android.ui.onboarding.password.recovery.newpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.PasswordRecoveryNewPasswordFragmentBinding
import ru.barteromeo.android.util.BaseFragment

@AndroidEntryPoint
class PasswordRecoveryNewPasswordFragment : BaseFragment() {

    companion object {
        fun newInstance() = PasswordRecoveryNewPasswordFragment()
    }

    private val viewModel: PasswordRecoveryNewPasswordViewModel by viewModels()
    private lateinit var binding: PasswordRecoveryNewPasswordFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PasswordRecoveryNewPasswordFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val emailText = arguments?.getString("email", "") ?: ""
        val codeText = arguments?.getString("code", "") ?: ""
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        binding.resetPasswordButton.setOnClickListener {
            val newPasswordText = binding.newPasswordEditText.text.toString()

            viewModel.onPasswordRecoverConfirmClick(emailText, codeText, newPasswordText)
        }
        binding.newPasswordEditText.addTextChangedListener {
            binding.newPasswordTextInputLayout.error = null
        }
        viewModel.recoverNewPasswordLiveData.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(
                    R.id.action_password_recovery_new_password_fragment_to_password_recovery_success_fragment
                )
            }
        }
    }

}