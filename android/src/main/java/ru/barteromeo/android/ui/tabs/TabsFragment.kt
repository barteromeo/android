package ru.barteromeo.android.ui.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.TabsFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.setupWithNavController

@AndroidEntryPoint
class TabsFragment : BaseFragment() {

    private lateinit var binding: TabsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TabsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {
        val navGraphIds = listOf(
            R.navigation.home_navigation,
//            R.navigation.category_search_flow_navigation,
            R.navigation.create_offer_flow_navigation,
            R.navigation.profile_flow_navigation,
            R.navigation.chat_flow_navigation,
        )

        // Setup the bottom navigation view with a list of navigation graphs
        binding.tabsNavView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = childFragmentManager,
            containerId = R.id.navHostTabsFragment,
            intent = requireActivity().intent
        )
    }

    fun navigateToHome() {
        binding.tabsNavView.selectedItemId = R.id.home_navigation
    }

    fun navigateToCreateOffer() {
        binding.tabsNavView.selectedItemId = R.id.create_offer_flow_navigation
    }

}