package ru.barteromeo.android.ui.onboarding.password.recovery.email

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.usecase.auth.PasswordRecoveryRequestUseCase
import javax.inject.Inject

@Reusable
class PasswordRecoveryRequestUseCase @Inject constructor(
    private val passwordRecoveryRequestUseCase: Lazy<PasswordRecoveryRequestUseCase>
) {

    suspend fun request(
        email: String,
    ): Flow<Boolean> {
        return passwordRecoveryRequestUseCase.get().execute(email)
    }

}