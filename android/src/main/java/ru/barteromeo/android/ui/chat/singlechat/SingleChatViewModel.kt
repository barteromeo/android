package ru.barteromeo.android.ui.chat.singlechat

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.android.util.GsonLocalDateTimeAdapter
import ru.barteromeo.data.UserPreferences
import ru.barteromeo.data.dto.MessageDto
import ru.barteromeo.domain.mapper.MessageMapper
import ru.barteromeo.domain.model.Author
import ru.barteromeo.domain.model.Message
import ru.barteromeo.domain.model.User
import ru.barteromeo.domain.usecase.chats.ResolveChatHistoryUseCase
import ru.barteromeo.domain.usecase.chats.ResolveChatListUseCase
import ru.barteromeo.domain.usecase.user.GetCurrentUserUseCase
import ru.barteromeo.domain.usecase.user.GetUserByIdUseCase
import timber.log.Timber
import ua.naiksoftware.stomp.Stomp
import ua.naiksoftware.stomp.StompClient
import ua.naiksoftware.stomp.dto.LifecycleEvent
import ua.naiksoftware.stomp.dto.StompHeader
import ua.naiksoftware.stomp.dto.StompMessage
import ua.naiksoftware.stomp.provider.OkHttpConnectionProvider.TAG
import java.io.Serializable
import java.lang.NullPointerException
import java.time.LocalDateTime
import java.util.*
import javax.inject.Inject

@HiltViewModel
class SingleChatViewModel @Inject constructor(
    private val userPreferences: UserPreferences,
    private val messageMapper: MessageMapper,
    private val resolveChatHistoryUseCase: ResolveChatHistoryUseCase,
    private val getCurrentUserUseCase: GetCurrentUserUseCase,
    private val resolveChatListUseCase: ResolveChatListUseCase,
    private val getUserByIdUseCase: GetUserByIdUseCase,
) : BaseViewModel() {
    companion object {
        const val SOCKET_URL = "ws://188.130.139.161:8080/ws"

        const val CHAT_LINK_SOCKET = "/chats/send"
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private val gson: Gson = GsonBuilder().registerTypeAdapter(
        LocalDateTime::class.java,
        GsonLocalDateTimeAdapter()
    ).create()
    private var mStompClient: StompClient? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var recipientId: Long? = null
    private var user: User? = null

    private val _chatState = MutableLiveData<Message?>()
    val liveChatState: LiveData<Message?> = _chatState

    private val recipientMutableLiveData: MutableLiveData<User> = MutableLiveData()
    val recipientLiveData: LiveData<User> = recipientMutableLiveData

    private val historyChatMutableLiveData = MutableLiveData<PagingData<Message>>()
    val historyChatLiveData: LiveData<PagingData<Message>> = historyChatMutableLiveData

    init {
        viewModelScope.launch {
            getCurrentUserUseCase.execute()
                .combine(userPreferences.accessToken) { user: User?, token: String? ->
                    user to token
                }.onEach { (user, token) ->
                    this@SingleChatViewModel.user = user
                    mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, SOCKET_URL)
                        .withServerHeartbeat(30000)
                    val headers = listOf(
                        StompHeader(
                            "Authorization",
                            token
                        )
                    )
                    resetSubscriptions()
                    initChat(headers)
                }.launchIn(this)

        }

    }

    fun resolveChatHistory(recipientId: Long) {
        this.recipientId = recipientId


        viewModelScope.launch {
            resolveChatListUseCase.execute().collect {
                val chat = it.firstOrNull { it.chatUser.id == recipientId }
                if (chat != null) {
                    resolveChatHistoryUseCase.resolveChatHistoryUseCase(chat.id).onEach {
                        historyChatMutableLiveData.value = it
                    }.catch {
                        mutableErrorLiveData.postValue(it)
                    }.launchIn(this)
                }
            }
            getUserByIdUseCase.execute(recipientId).catch {
                mutableErrorLiveData.postValue(it)
            }.collect {
                if (it == null) {
                    mutableErrorLiveData.postValue(NullPointerException("User not found"))
                } else {
                    recipientMutableLiveData.postValue(it)
                }
            }
        }
    }

    private fun initChat(headers: List<StompHeader>) {
        resetSubscriptions()

        if (mStompClient != null) {
            val topicSubscribe = mStompClient!!.topic("/user/${user?.login}/queue/messages")
                .subscribeOn(Schedulers.io(), false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ topicMessage: StompMessage ->
                    Timber.d(topicMessage.payload)
                    val message: MessageDto =
                        gson.fromJson(topicMessage.payload, MessageDto::class.java)
                    val newMessage = messageMapper.mapForLiveChats(message)
                    addMessage(newMessage)
                },
                    {
                        Timber.e(it, "Error!")
                    }
                )

            val lifecycleSubscribe = mStompClient!!.lifecycle()
                .subscribeOn(Schedulers.io(), false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ lifecycleEvent: LifecycleEvent ->
                    when (lifecycleEvent.type!!) {
                        LifecycleEvent.Type.OPENED -> Timber.d("Stomp connection opened")
                        LifecycleEvent.Type.ERROR -> Timber.e(lifecycleEvent.exception, "Error")
                        LifecycleEvent.Type.FAILED_SERVER_HEARTBEAT,
                        LifecycleEvent.Type.CLOSED -> {
                            Timber.d("Stomp connection closed")
                        }
                    }
                }, {
                    Timber.e(it)
                })

            compositeDisposable!!.add(lifecycleSubscribe)
            compositeDisposable!!.add(topicSubscribe)

            if (!mStompClient!!.isConnected) {
                mStompClient!!.connect(headers)
            }


        } else {
            Timber.e("mStompClient is null!")
        }
    }

    fun sendMessage(text: String) {
        val message = ChatMessageSent(recipientId!!, text, emptyList())
        sendCompletable(mStompClient!!.send(CHAT_LINK_SOCKET, gson.toJson(message)))
        addMessage(
            Message(
                emptyList(),
                null,
                Date(),
                false,
                null,
                message.message,
                user?.id?.toLong()
            )
        )
    }

    private fun addMessage(message: Message) {
        _chatState.value = message
    }

    private fun sendCompletable(request: Completable) {
        compositeDisposable?.add(
            request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Timber.tag(TAG).d("Stomp sended")
                    },
                    {
                        Timber.tag(TAG).e(it, "Stomp error")
                    }
                )
        )
    }

    private fun resetSubscriptions() {
        if (compositeDisposable != null) {
            compositeDisposable!!.dispose()
        }

        compositeDisposable = CompositeDisposable()
    }

    override fun onCleared() {
        super.onCleared()
        mStompClient?.disconnect()
    }

    private data class ChatMessageSent(
        @SerializedName("recipientId") val recipientId: Long,
        @SerializedName("message") val message: String,
        @SerializedName("attachmentIds") val attachments: List<Long>,
    ) : Serializable {
        companion object {
            private const val serialVersionUID = 1L
        }
    }
}