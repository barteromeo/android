package ru.barteromeo.android.ui.onboarding.password.recovery.code

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PasswordRecoveryCodeViewModel @Inject constructor(
    private val passwordRecoveryConfirmUseCase: PasswordRecoveryConfirmUseCases
) : BaseViewModel() {
    val recoveryCodeLiveData: LiveData<Boolean>
        get() = recoveryCodeMutableLiveData

    private val recoveryCodeMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun onPasswordRecoverConfirmClick(
        email: String,
        code: String,
    ) {
        viewModelScope.launch {
            passwordRecoveryConfirmUseCase.confirm(email, code).catch {
                mutableErrorLiveData.postValue(it)
            }.onEach {
                recoveryCodeMutableLiveData.postValue(it)
            }
                .launchIn(this)
        }
    }

    fun onResendCodeClick(
        email: String
    ) {
        viewModelScope.launch {
            passwordRecoveryConfirmUseCase.request(email).catch {
                mutableErrorLiveData.postValue(it)
            }.onEach {
                recoveryCodeMutableLiveData.postValue(it)
            }.launchIn(this)
        }
    }
}