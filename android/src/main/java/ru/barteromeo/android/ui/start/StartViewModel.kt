package ru.barteromeo.android.ui.start

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.android.util.SingleLiveEvent
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class StartViewModel @Inject constructor(
    private val startUseCases: StartUseCases
) : BaseViewModel() {

    private val _startNavCommand = SingleLiveEvent<StartNavCommand?>()

    val startNavCommand: LiveData<StartNavCommand?> = _startNavCommand

    init {
        // Add special delay for splash screen
        viewModelScope.launch {
            startUseCases.checkAuthPresent().catch {
                _startNavCommand.postValue(StartNavCommand.NAVIGATE_TO_AUTH)
                Timber.i("Auth caught")
            }.collect {
                val navCommand = if (it) {
                    StartNavCommand.NAVIGATE_TO_TABS
                } else {
                    StartNavCommand.NAVIGATE_TO_AUTH
                }
                Timber.i("Auth collected")
                _startNavCommand.postValue(navCommand)
            }
        }
    }
}