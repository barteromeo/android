package ru.barteromeo.android.ui.createoffer.details

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.databinding.AddPhotoItemBinding

class SelectPhotoAdapterItem(
    private val onClick: () -> Unit,
) : AbstractBindingItem<AddPhotoItemBinding>() {

    override val type: Int = -2

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): AddPhotoItemBinding {
        return AddPhotoItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: AddPhotoItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.root.setOnClickListener {
            onClick.invoke()
        }
    }
}