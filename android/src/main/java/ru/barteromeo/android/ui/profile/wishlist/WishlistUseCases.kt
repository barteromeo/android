package ru.barteromeo.android.ui.profile.wishlist

import androidx.paging.PagingData
import dagger.Reusable
import dagger.Lazy
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.SearchOffer
import ru.barteromeo.domain.usecase.wishlist.AddToWishlistUseCase
import ru.barteromeo.domain.usecase.wishlist.GetWishlistUseCase
import ru.barteromeo.util.data.Resource
import javax.inject.Inject

@Reusable
class WishlistUseCases @Inject constructor(
    private val getWishlistUseCase: Lazy<GetWishlistUseCase>,
    private val addToWishlistUseCase: Lazy<AddToWishlistUseCase>,
) {

    suspend fun getWishList(): Flow<PagingData<SearchOffer>> {
        return getWishlistUseCase.get().execute()
    }

    suspend fun addToWishlist(id: Long, inWishlist: Boolean): Flow<Resource<*>> {
        return addToWishlistUseCase.get().execute(id, inWishlist)
    }

}