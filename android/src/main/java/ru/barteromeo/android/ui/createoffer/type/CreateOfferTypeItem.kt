package ru.barteromeo.android.ui.createoffer.type

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.databinding.OfferTypeItemBinding
import ru.barteromeo.domain.model.OfferType

class CreateOfferTypeItem(
    private val offerType: OfferType,
    private val onClickCallback: (OfferType) -> Unit,
) : AbstractBindingItem<OfferTypeItemBinding>() {
    override val type: Int
        get() = -1

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): OfferTypeItemBinding {
        return OfferTypeItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: OfferTypeItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.offerTypeLabelTextView.text = offerType.type
        binding.root.setOnClickListener {
            onClickCallback(offerType)
        }
    }
}