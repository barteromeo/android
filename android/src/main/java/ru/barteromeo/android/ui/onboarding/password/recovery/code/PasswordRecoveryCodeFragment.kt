package ru.barteromeo.android.ui.onboarding.password.recovery.code

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.PasswordRecoveryCodeFragmentBinding
import ru.barteromeo.android.util.BaseFragment

@AndroidEntryPoint
class PasswordRecoveryCodeFragment : BaseFragment() {

    companion object {
        fun newInstance() = PasswordRecoveryCodeFragment()
    }

    private val viewModel: PasswordRecoveryCodeViewModel by viewModels()
    private lateinit var binding: PasswordRecoveryCodeFragmentBinding
    private var code = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PasswordRecoveryCodeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun sendRecoverCode(
        emailText: String
    ) {
        val codeText = binding.passwordRecoveryCodeEditText.text.toString()
        code = codeText
        viewModel.onPasswordRecoverConfirmClick(emailText, codeText)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val emailText = arguments?.getString("email", "") ?: ""
        binding.passwordRecoveryCodeSendButton.setOnClickListener {
            sendRecoverCode(emailText)
        }
        binding.passwordRecoveryCodeResendButton.setOnClickListener {
            viewModel.onResendCodeClick(emailText)
        }
        binding.passwordRecoveryCodeEditText.addTextChangedListener {
            binding.passwordRecoveryCodeTextInputLayout.error = null
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.recoveryCodeLiveData.observe(viewLifecycleOwner) {
            val bundle = bundleOf(
                "email" to emailText,
                "code" to code
            )
            if (it) {
                findNavController().navigate(
                    R.id.action_password_recovery_code_fragment_to_password_recovery_new_password_fragment,
                    bundle
                )
            }
        }
    }

}