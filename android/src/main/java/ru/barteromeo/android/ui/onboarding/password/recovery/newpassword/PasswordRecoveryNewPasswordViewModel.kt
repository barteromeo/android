package ru.barteromeo.android.ui.onboarding.password.recovery.newpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PasswordRecoveryNewPasswordViewModel @Inject constructor(
    private val passwordRecoveryUseCase: PasswordRecoveryUseCase
) : BaseViewModel() {
    val recoverNewPasswordLiveData: LiveData<Boolean>
        get() = recoverNewPasswordMutableLiveData

    private val recoverNewPasswordMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun onPasswordRecoverConfirmClick(
        email: String,
        code: String,
        newPassword: String,
    ) {
        viewModelScope.launch {
            passwordRecoveryUseCase.recovery(email, code, newPassword).catch {
                mutableErrorLiveData.postValue(it)
            }.onEach {
                recoverNewPasswordMutableLiveData.postValue(it)
            }
                .launchIn(this)
        }
    }
}