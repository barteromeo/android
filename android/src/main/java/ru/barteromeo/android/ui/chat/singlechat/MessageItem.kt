package ru.barteromeo.android.ui.chat.singlechat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.MessageItemBinding
import ru.barteromeo.android.util.setTextOrGone
import ru.barteromeo.domain.model.Message

class MessageItem(private val message: Message, private val recipientId: Long) :
    AbstractBindingItem<MessageItemBinding>() {
    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): MessageItemBinding {
        return MessageItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: MessageItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.messageTextView.setTextOrGone(message.message)
        if (message.userId != null) {
            binding.messageTextView.setBackgroundColor(binding.root.resources.getColor(R.color.primary))
        }
    }

    override val type: Int
        get() = -1
}