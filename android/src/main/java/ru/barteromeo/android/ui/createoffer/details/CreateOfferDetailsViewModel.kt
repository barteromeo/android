package ru.barteromeo.android.ui.createoffer.details

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.android.vo.CategoryVo
import ru.barteromeo.domain.model.OfferType
import java.lang.IllegalArgumentException
import javax.inject.Inject

@HiltViewModel
class CreateOfferDetailsViewModel @Inject constructor(
    private val createOfferDetailsUseCases: CreateOfferDetailsUseCases
) : BaseViewModel() {

    private val photosMutableLiveData: MutableLiveData<List<PhotoVo>> = MutableLiveData(emptyList())
    val photosLiveData: LiveData<List<PhotoVo>>
        get() = photosMutableLiveData

    private fun uploadPhoto(uri: Uri) {
        viewModelScope.launch {
            createOfferDetailsUseCases.uploadPhoto(uri)
                .onEach {
                    synchronized(photosLock) {
                        val list = photosMutableLiveData.value?.toMutableList() ?: mutableListOf()
                        val photoVoIndex = list.indexOfFirst { photoVo ->
                            when (photoVo) {
                                is PhotoVo.Local -> {
                                    photoVo.uri == uri
                                }
                                else -> {
                                    false
                                }
                            }
                        }
                        list[photoVoIndex] = PhotoVo.Local(uri, PhotoVo.Local.State.LOADED, it.id)
                        photosMutableLiveData.value = list
                    }
                }
                .catch {
                    mutableErrorLiveData.postValue(it)
                }
                .launchIn(this)
        }
    }

    fun uploadPhotos(uris: List<Uri>) {
        val list = photosMutableLiveData.value?.toMutableList() ?: mutableListOf()
        list.addAll(uris.map { PhotoVo.Local(it, PhotoVo.Local.State.LOADING, null) }.filter {
            list.find { photoVo -> (photoVo as? PhotoVo.Local)?.uri == it.uri } == null
        })
        photosMutableLiveData.value = list
        uris.forEach { uploadPhoto(it) }
    }

    fun createOffer(
        details: List<DetailType>,
        tags: List<CategoryVo>,
        type: OfferType,
        callback: () -> Unit,
    ) {

        viewModelScope.launch {
            val list = photosMutableLiveData.value ?: emptyList()
            val photoIds = list.mapNotNull {
                val photoVo = (it as? PhotoVo.Local)
                if (photoVo?.state == PhotoVo.Local.State.LOADED) {
                    photoVo.photoId?.toInt()
                } else {
                    null
                }
            }



            if (photoIds.isNotEmpty() && !details.contains(DetailType.Empty)) {
                val title = details.first { it is DetailType.Title }.value
                val description = details.first { it is DetailType.Description }.value
                val changeOn = details.first { it is DetailType.ExchangeFor }.value
                val phone = details.first { it is DetailType.Phone }.value
                val whereToMeet = details.first { it is DetailType.WhereToMeet }.value
                createOfferDetailsUseCases.createOffer(
                    title = title,
                    photoIds = photoIds,
                    description = description,
                    changeOn = changeOn,
                    phone = phone,
                    whereToMeet = whereToMeet,
                    tags = emptyList(),
                    type = type,
                    categoryIds = tags.map { it.id },
                )
                callback()
            } else {
                mutableErrorLiveData.postValue(
                    IllegalArgumentException("Необходимо прикрепить хотя бы одну фотографию")
                )
            }
        }
    }

    fun clearState() {
        photosMutableLiveData.value = emptyList()
    }

    companion object {
        private val photosLock = Object()
    }
}