package ru.barteromeo.android.ui.start

enum class StartNavCommand {
    NAVIGATE_TO_TABS,
    NAVIGATE_TO_AUTH
}