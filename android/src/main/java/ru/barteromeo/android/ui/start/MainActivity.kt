package ru.barteromeo.android.ui.start

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)