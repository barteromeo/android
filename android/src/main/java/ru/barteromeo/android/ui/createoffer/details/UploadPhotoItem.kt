package ru.barteromeo.android.ui.createoffer.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import ru.barteromeo.android.databinding.CreateOfferPhotoItemBinding
import ru.barteromeo.android.util.gone

class UploadPhotoItem(
    private val requestManager: RequestManager,
    model: PhotoVo,
    private val onPhotoRemove: (PhotoVo) -> Unit,
    private val onPhotoReload: (PhotoVo.Local) -> Unit
) :
    ModelAbstractBindingItem<PhotoVo, CreateOfferPhotoItemBinding>(model) {

    override val type: Int
        get() = -1


    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): CreateOfferPhotoItemBinding {
        return CreateOfferPhotoItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: CreateOfferPhotoItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        when (val constantModel = model) {
            is PhotoVo.Local -> bindLocal(binding, constantModel)
            is PhotoVo.Remote -> bindRemote(binding, constantModel)
        }
        binding.imageOfferPhotoRemove.setOnClickListener {
            onPhotoRemove.invoke(model)
        }
    }


    private fun bindRemote(binding: CreateOfferPhotoItemBinding, remote: PhotoVo.Remote) {
        with(binding) {
            requestManager
                .load(
                    remote.orig
                ).transform(CenterCrop())
                .into(imageOfferPhotoAdd)
            viewOfferPhotoLoadFailed.gone()
            viewOfferPhotoLoading.gone()

        }
    }

    private fun bindLocal(binding: CreateOfferPhotoItemBinding, local: PhotoVo.Local) {
        with(binding) {
            requestManager.load(local.uri).transform(
//                CenterCrop(),
                FitCenter()
            )
                .into(imageOfferPhotoAdd)
            viewOfferPhotoLoadFailed.isVisible =
                local.state == PhotoVo.Local.State.LOAD_FAILED
            viewOfferPhotoLoading.isVisible = local.state == PhotoVo.Local.State.LOADING
            viewOfferPhotoLoadFailed.setOnClickListener {
                onPhotoReload.invoke(local)
            }

        }
    }


    override fun unbindView(binding: CreateOfferPhotoItemBinding) {
        super.unbindView(binding)
        with(binding) {
            viewOfferPhotoLoadFailed.setOnClickListener(null)
            imageOfferPhotoRemove.setOnClickListener(null)
        }
    }
}