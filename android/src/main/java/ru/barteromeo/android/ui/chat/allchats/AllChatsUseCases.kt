package ru.barteromeo.android.ui.chat.allchats

import dagger.Reusable
import javax.inject.Inject
import dagger.Lazy
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Chat
import ru.barteromeo.domain.usecase.chats.ResolveChatListUseCase

@Reusable
class AllChatsUseCases @Inject constructor(
    private val getAllChatsListUseCase: Lazy<ResolveChatListUseCase>
) {

    suspend fun getAllChats(): Flow<List<Chat>> {
        return getAllChatsListUseCase.get().execute()
    }

}