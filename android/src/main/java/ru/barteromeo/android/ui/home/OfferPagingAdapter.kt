package ru.barteromeo.android.ui.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import ru.barteromeo.android.databinding.ItemHomeOfferBinding
import ru.barteromeo.android.util.gone
import ru.barteromeo.android.util.inflateChip
import ru.barteromeo.android.vo.SearchOfferVo

class OfferPagingAdapter(
    private val onItemClicked: (Long) -> Unit,
    private val onFavouriteClicked: (Long, Boolean, Int) -> Unit,
    private val requestManager: RequestManager,
) :
    PagingDataAdapter<SearchOfferVo, OfferPagingAdapter.SearchOfferViewHolder>(
        OffersDiffCallback()
    ) {


    override fun onBindViewHolder(holder: SearchOfferViewHolder, position: Int) {

        val data = getItem(position)

        holder.bind(data)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchOfferViewHolder {

        return SearchOfferViewHolder(
            ItemHomeOfferBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    inner class SearchOfferViewHolder(
        private val binding: ItemHomeOfferBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: SearchOfferVo?) {
            if (data == null) {
                binding.root.gone()
            } else {
                binding.apply {

                    root.setOnClickListener {
                        onItemClicked.invoke(data.id)
                    }
                    offerAddToFavourites.setOnClickListener {
                        onFavouriteClicked.invoke(
                            data.id,
                            offerAddToFavourites.isSelected,
                            layoutPosition
                        )
                        offerAddToFavourites.isSelected = !offerAddToFavourites.isSelected
                    }
                    offerAddToFavourites.isSelected = data.inWishlist

                    offerTitleTextView.text = data.title
                    offerAddressTextView.text = data.address
                    val layoutInflater = LayoutInflater.from(root.context)
                    offerCategoryChipGroup.removeAllViews()
                    data.categories.forEach {
                        offerCategoryChipGroup.addView(
                            inflateChip(
                                layoutInflater,
                                offerCategoryChipGroup,
                                it.value
                            )
                        )
                    }
                    requestManager.load(
                        Uri.parse(data.photos.first())
                    ).centerCrop()
                        .into(offerPhotoView)
                }
            }
        }
    }

    private class OffersDiffCallback : DiffUtil.ItemCallback<SearchOfferVo>() {
        override fun areItemsTheSame(oldItem: SearchOfferVo, newItem: SearchOfferVo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: SearchOfferVo, newItem: SearchOfferVo): Boolean {
            return oldItem == newItem
        }
    }

}