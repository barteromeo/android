package ru.barteromeo.android.ui.chat.allchats

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.domain.model.Chat
import javax.inject.Inject

@HiltViewModel
class AllChatsViewModel @Inject constructor(
    private val allChatsUseCases: AllChatsUseCases,
) : BaseViewModel() {

    private val chatsMutableLiveData: MutableLiveData<List<Chat>> = MutableLiveData()

    val chatsLiveData: LiveData<List<Chat>> = chatsMutableLiveData


    fun getAllChats() {
        viewModelScope.launch {
            allChatsUseCases.getAllChats().onEach {
                chatsMutableLiveData.postValue(it)
            }.catch {
                mutableErrorLiveData.postValue(it)
            }.launchIn(this)
        }
    }

}