package ru.barteromeo.android.ui.profile.phone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.databinding.ChangePhoneFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.InputTextRecyclerItem

@AndroidEntryPoint
class ChangePhoneFragment : BaseFragment() {

    companion object {
        fun newInstance() = ChangePhoneFragment()
    }

    private val viewModel: ChangePhoneViewModel by viewModels()
    private lateinit var binding: ChangePhoneFragmentBinding
    private val detailAdapter = FastItemAdapter<InputTextRecyclerItem<String>>().apply {
        setHasStableIds(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ChangePhoneFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.userLiveData.observe(viewLifecycleOwner) {
            if (it != null) {
                setUpDetails(it.phone)
            }
        }
        viewModel.fetchUser()
    }

    private fun setUpDetails(phone: String?) {
        val detailItems = mutableListOf<InputTextRecyclerItem<String>>()
        with(detailItems) {
            add(
                InputTextRecyclerItem("Телефон", phone) {
                    it.orEmpty()
                }
            )
        }
        detailAdapter.itemAdapter.set(detailItems)
        binding.changePhoneRecyclerView.adapter = detailAdapter
        binding.changePhoneRecyclerView.layoutManager = LinearLayoutManager(
            requireContext(), LinearLayoutManager.VERTICAL,
            false
        )
        binding.changePhoneButton.setOnClickListener {
            viewModel.changePhone(
                phone = detailItems.first().produce(),
            )
        }
    }

}