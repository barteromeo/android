package ru.barteromeo.android.ui.onboarding.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R

@AndroidEntryPoint
class OnBoardingCategoryFragment : Fragment() {

    companion object {
        fun newInstance() = OnBoardingCategoryFragment()
    }


    private val viewModel: OnBoardingCategoryViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.on_boarding_category_fragment, container, false)
    }

}