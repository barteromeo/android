package ru.barteromeo.android.ui.category.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.databinding.CategoryStartFragmentBinding
import ru.barteromeo.android.util.BaseFragment

@AndroidEntryPoint
class CategoryStartFragment : BaseFragment() {

    companion object {
        fun newInstance() = CategoryStartFragment()
    }

    private val viewModel: CategoryStartViewModel by viewModels()
    private lateinit var binding: CategoryStartFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CategoryStartFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

}