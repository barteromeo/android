package ru.barteromeo.android.ui.createoffer.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.ui.offer.CategoryFormatter
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.android.vo.CategoryVo
import javax.inject.Inject

@HiltViewModel
class CreateOfferCategoriesViewModel @Inject constructor(
    private val useCases: CreateOfferCategoriesUseCases,
    private val categoryFormatter: CategoryFormatter,
) : BaseViewModel() {

    val categoryLiveData: LiveData<List<CategoryVo>>
        get() = categoryMutableLiveData

    private val categoryMutableLiveData = MutableLiveData<List<CategoryVo>>()

    fun loadCategories() {
        viewModelScope.launch {
            useCases.resolveCategories().catch {
                mutableErrorLiveData.postValue(it)
            }.onEach {
                categoryMutableLiveData.value =
                    it.map { category -> categoryFormatter.format(category) }
            }.launchIn(this)
        }
    }

}