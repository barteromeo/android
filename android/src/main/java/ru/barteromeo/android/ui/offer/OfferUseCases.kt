package ru.barteromeo.android.ui.offer

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.Offer
import ru.barteromeo.domain.usecase.offer.GetOfferByIdUseCase
import javax.inject.Inject

@Reusable
class OfferUseCases @Inject constructor(
    private val getOfferByIdUseCase: Lazy<GetOfferByIdUseCase>
) {

    suspend fun getOfferById(offerId: Long): Flow<Offer> {
        return getOfferByIdUseCase.get().execute(offerId)
    }

}