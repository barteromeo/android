package ru.barteromeo.android.ui.profile.myoffers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.ui.home.SearchFormatter
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.android.vo.SearchOfferVo
import ru.barteromeo.domain.usecase.offer.GetMyOffersUseCase
import ru.barteromeo.domain.usecase.offer.GetOffersUseCase
import ru.barteromeo.domain.usecase.wishlist.AddToWishlistUseCase
import ru.barteromeo.util.data.Resource
import javax.inject.Inject

@HiltViewModel
class MyOffersViewModel @Inject constructor(
    private val getOffersUseCase: GetMyOffersUseCase,
    private val addToWishlistUseCase: AddToWishlistUseCase,
    private val offersFormatter: SearchFormatter,
) : BaseViewModel() {

    val offersLiveData: LiveData<PagingData<SearchOfferVo>>
        get() = mutableOffersLiveData

    private val mutableOffersLiveData =
        MutableLiveData<PagingData<SearchOfferVo>>(PagingData.empty())

    fun getOffers() {
        viewModelScope.launch {
            getOffersUseCase.execute()
                .catch {
                    mutableErrorLiveData.postValue(it)
                }
                .onEach { pagingData ->
                    mutableOffersLiveData.value = pagingData.map { offersFormatter.format(it) }
                }
                .cachedIn(this)
                .launchIn(this)
        }
    }

    fun addToWishlist(id: Long, inWishlist: Boolean) {
        viewModelScope.launch {
            addToWishlistUseCase.execute(id, inWishlist).onEach {
                when (it) {
                    is Resource.Success -> {
                        // todo: toggle heart picture
                    }
                    is Resource.Failure -> {
                        mutableErrorLiveData.postValue(it.throwable)
                    }
                    else -> {
                        //no-op
                    }
                }
            }.catch {
                mutableErrorLiveData.postValue(it)
            }
                .launchIn(this)
        }
    }

}