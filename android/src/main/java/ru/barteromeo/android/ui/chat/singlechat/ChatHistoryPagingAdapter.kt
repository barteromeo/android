package ru.barteromeo.android.ui.chat.singlechat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.MessageItemBinding
import ru.barteromeo.android.util.gone
import ru.barteromeo.android.util.setTextOrGone
import ru.barteromeo.domain.model.Message

class ChatHistoryPagingAdapter(private val recipientId: Long) :
    PagingDataAdapter<Message, ChatHistoryPagingAdapter.MessageViewHolder>(
        OffersDiffCallback()
    ) {


    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {

        return MessageViewHolder(
            MessageItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    inner class MessageViewHolder(
        private val binding: MessageItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Message?) {
            if (data == null) {
                binding.root.gone()
            } else {
                binding.apply {
                    messageTextView.setTextOrGone(data.message)
                    if (data.userId != recipientId) {
                        binding.messageTextView.setBackgroundColor(binding.root.resources.getColor(R.color.primary))
                    }
                }
            }
        }
    }

    private class OffersDiffCallback : DiffUtil.ItemCallback<Message>() {
        override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem.message == newItem.message && oldItem.messageId == newItem.messageId
        }

        override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
            return oldItem == newItem
        }
    }

}