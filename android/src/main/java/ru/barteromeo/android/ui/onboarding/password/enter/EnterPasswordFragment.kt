package ru.barteromeo.android.ui.onboarding.password.enter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.EnterPasswordFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import timber.log.Timber

@AndroidEntryPoint
class EnterPasswordFragment : BaseFragment() {

    companion object {
        fun newInstance() = EnterPasswordFragment()
    }

    private val viewModel: EnterPasswordViewModel by viewModels()
    private lateinit var binding: EnterPasswordFragmentBinding
    private lateinit var email: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = EnterPasswordFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        email = arguments?.getString("email", "") ?: ""
        binding.passwordSubtitle.text = getString(R.string.enter_password_subtitle, email)
        binding.passwordNextButton.setOnClickListener {
            val text = binding.passwordEditText.text.toString()
            if (text.isNotBlank()) {
                viewModel.login(email, text)
            } else {
                binding.passwordTextInputLayout.error = getString(R.string.password_is_empty)
            }
        }
        binding.passwordEditText.addTextChangedListener {
            binding.passwordEditText.error = null
        }
        binding.forgotPassword.setOnClickListener {
            findNavController().navigate(
                R.id.action_enter_password_fragment_to_navigation_recovery
            )
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.loggedInLiveData.observe(viewLifecycleOwner) { loggedIn ->
            Timber.d("Observed")
            if (loggedIn) {
                findNavController().navigate(R.id.action_navigate_to_tabs)
            } else {
                binding.passwordTextInputLayout.error = getString(R.string.wrong_password)
            }
        }
    }

}