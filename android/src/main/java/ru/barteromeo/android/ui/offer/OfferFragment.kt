package ru.barteromeo.android.ui.offer

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.RequestManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.OfferFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.gone
import ru.barteromeo.android.util.inflateChip
import ru.barteromeo.android.util.visible
import ru.barteromeo.android.vo.OfferVo
import javax.inject.Inject

@AndroidEntryPoint
class OfferFragment : BaseFragment() {

    companion object {
        fun newInstance(): OfferFragment {
            return OfferFragment()
        }
    }


    private val viewModel: OfferViewModel by viewModels()
    private val viewPagerAdapter = ItemAdapter<OfferGalleryItem>()
    private lateinit var binding: OfferFragmentBinding

    @Inject
    lateinit var requestManager: RequestManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = OfferFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getLong("id")?.let { viewModel.getOffer(it) }
        viewModel.searchOfferLiveData.observe(viewLifecycleOwner) {
            showOffer(it)
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
    }

    private fun showOffer(offerVo: OfferVo) {
        binding.offerProgressBar.gone()
        binding.offerContainer.visible()
        bindGallery(offerVo)
        bindOfferDescription(offerVo)
        bindOfferAuthor(offerVo)
        bindOfferFactors(offerVo)
        bindOfferButton(offerVo)
    }

    private fun bindGallery(offerVo: OfferVo) {
        viewPagerAdapter.add(offerVo.photos.mapNotNull {
            if (it.isNotBlank()) {
                OfferGalleryItem(
                    requestManager = requestManager,
                    url = it
                )
            } else {
                null
            }
        })
        binding.offerGalleryViewPager.adapter = FastAdapter.with(viewPagerAdapter)
    }

    private fun bindOfferDescription(offerVo: OfferVo) {
        with(binding) {
            offerTitleTextView.text = offerVo.title
            offerDescriptionTextView.text = offerVo.description
            offerTimeTextView.text = offerVo.updated.toString()
            offerCategoryChipGroup.removeAllViews()
            offerVo.categories.forEach {
                offerCategoryChipGroup.addView(
                    inflateChip(
                        layoutInflater,
                        offerCategoryChipGroup,
                        it.value
                    )
                )
            }
        }
    }

    private fun bindOfferAuthor(offerVo: OfferVo) {
        with(binding) {
            requestManager.load(offerVo.userPhoto).placeholder(R.drawable.ic_twotone_person_24)
                .centerCrop().into(offerAuthorAvatarImageView)
            offerAuthorNameTextView.text = offerVo.userName
            offerAddressTextView.text = offerVo.address
        }
    }

    private fun bindOfferFactors(offerVo: OfferVo) {
        binding.offerFactorsLinearLayout.removeAllViews()
        offerVo.factors.forEach {
            if (it.content.isNotBlank()) {
                val view = SimpleFactorView(requireContext())
                binding.offerFactorsLinearLayout.addView(view)
                view.showVo(it)
            }
        }
        binding.offerFactorsLinearLayout.invalidate()
    }

    private fun bindOfferButton(offerVo: OfferVo) {
        binding.makeOfferButton.setOnClickListener {
//            onCallClicked(offerVo)
            findNavController().navigate(
                R.id.navigate_to_single_chat,
                bundleOf("recipientId" to offerVo.authorId)
            )
        }
    }

    private fun onCallClicked(offerVo: OfferVo) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + offerVo.phone))
        startActivity(intent)
    }


}