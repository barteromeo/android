package ru.barteromeo.android.ui.onboarding.register.code

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.usecase.auth.ConfirmEmailCodeUseCase
import ru.barteromeo.domain.usecase.auth.RegisterUseCase
import javax.inject.Inject

@Reusable
class ConfirmEmailCodeUseCases @Inject constructor(
    private val registerUseCase: Lazy<RegisterUseCase>,
    private val confirmEmailCodeUseCase: Lazy<ConfirmEmailCodeUseCase>,
) {

    suspend fun register(
        email: String,
        name: String,
        surname: String,
        password: String,
        code: String,
    ): Flow<Boolean> {
        return registerUseCase.get().execute(email, password, name, surname, code)
    }

    suspend fun confirm(
        code: String,
        email: String,
    ): Flow<Boolean> {
        return confirmEmailCodeUseCase.get().execute(code, email)
    }

}