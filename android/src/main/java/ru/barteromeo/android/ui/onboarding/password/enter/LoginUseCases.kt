package ru.barteromeo.android.ui.onboarding.password.enter

import dagger.Reusable
import ru.barteromeo.domain.usecase.auth.LoginUseCase
import javax.inject.Inject
import dagger.Lazy
import kotlinx.coroutines.flow.Flow

@Reusable
class LoginUseCases @Inject constructor(
    private val loginUseCase: Lazy<LoginUseCase>
) {

    suspend fun login(username: String, password: String): Flow<Boolean> {
        return loginUseCase.get().execute(username, password)
    }

}