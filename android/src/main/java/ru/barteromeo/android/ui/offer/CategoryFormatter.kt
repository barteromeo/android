package ru.barteromeo.android.ui.offer

import dagger.Reusable
import ru.barteromeo.android.vo.CategoryVo
import ru.barteromeo.domain.model.Category
import javax.inject.Inject

@Reusable
class CategoryFormatter @Inject constructor() {

    fun format(category: Category): CategoryVo {
        return CategoryVo(
            id = category.id,
            value = category.value,
            photo = category.photo
        )
    }

}