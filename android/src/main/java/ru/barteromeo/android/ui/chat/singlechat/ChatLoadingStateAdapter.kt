package ru.barteromeo.android.ui.chat.singlechat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.barteromeo.android.databinding.NetworkStateItemBinding
import ru.barteromeo.android.util.gone
import ru.barteromeo.android.util.visible

class ChatLoadingStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<ChatLoadingStateAdapter.LoadStateViewHolder>() {

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {

        val progress = holder.binding.progressBarItem
        val retryBtn = holder.binding.retyBtn
        val txtErrorMessage = holder.binding.errorMsgItem

        if (loadState is LoadState.Loading) {
            progress.visible()
            txtErrorMessage.gone()
            retryBtn.gone()

        } else {
            progress.gone()
        }

        if (loadState is LoadState.Error) {
            txtErrorMessage.visible()
            retryBtn.visible()
            txtErrorMessage.text = loadState.error.localizedMessage
        }


        retryBtn.setOnClickListener {
            retry.invoke()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        return LoadStateViewHolder(
            NetworkStateItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    inner class LoadStateViewHolder(val binding: NetworkStateItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}