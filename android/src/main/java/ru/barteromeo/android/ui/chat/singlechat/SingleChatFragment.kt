package ru.barteromeo.android.ui.chat.singlechat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.SingleChatFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import javax.inject.Inject

@AndroidEntryPoint
class SingleChatFragment : BaseFragment() {

    companion object {
        fun newInstance() = SingleChatFragment()
    }

    @Inject
    lateinit var requestManager: RequestManager

    private lateinit var binding: SingleChatFragmentBinding

    private lateinit var historyPagingAdapter: ChatHistoryPagingAdapter
    private val liveChatItemAdapter = ItemAdapter<MessageItem>()
    private val liveChatAdapter = FastAdapter.with(liveChatItemAdapter)
    private lateinit var adapter: ConcatAdapter
    private val viewModel: SingleChatViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SingleChatFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        viewModel.recipientLiveData.observe(viewLifecycleOwner) {
            requestManager.load(it.photo).placeholder(R.drawable.ic_twotone_person_24)
                .into(binding.avatarImageView)
            binding.recipientNameTextView.text = "${it.name} ${it.surname}"
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
    }

    private fun setUpAdapter() {
        val recipientId: Long = arguments?.getLong("recipientId") ?: throw Exception()
        viewModel.resolveChatHistory(recipientId)
        historyPagingAdapter = ChatHistoryPagingAdapter(recipientId)
        adapter = historyPagingAdapter.withLoadStateHeader(
            header = ChatLoadingStateAdapter {
                historyPagingAdapter.retry()
            }
        )
        adapter.addAdapter(0, liveChatAdapter)



        viewModel.historyChatLiveData.observe(viewLifecycleOwner) {
            historyPagingAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }
        viewModel.liveChatState.observe(viewLifecycleOwner) {
            it?.let {
                liveChatItemAdapter.add(0, MessageItem(it, recipientId))
            }
        }
        binding.chatRecyclerView.adapter = adapter
        binding.chatRecyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, true)
        binding.sendMessageButton.setOnClickListener {
            viewModel.sendMessage(binding.messageInputEditText.text.toString())
            binding.messageInputEditText.setText("", TextView.BufferType.EDITABLE)
            binding.messageInputEditText.clearFocus()
        }

    }


}