package ru.barteromeo.android.ui.createoffer.details

import android.net.Uri
import dagger.Lazy
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.data.dto.UploadResponseDto
import ru.barteromeo.domain.model.OfferType
import ru.barteromeo.domain.usecase.createoffer.CreateOfferUseCase
import ru.barteromeo.domain.usecase.resource.UploadResourceUseCase
import javax.inject.Inject

class CreateOfferDetailsUseCases @Inject constructor(
    private val uploadResourceUseCase: Lazy<UploadResourceUseCase>,
    private val createOfferUseCase: Lazy<CreateOfferUseCase>,
) {

    suspend fun uploadPhoto(uri: Uri): Flow<UploadResponseDto> {
        return uploadResourceUseCase.get().execute(uri)
    }

    suspend fun createOffer(
        title: String,
        photoIds: List<Int>,
        description: String,
        changeOn: String,
        phone: String,
        whereToMeet: String,
        tags: List<String>,
        type: OfferType,
        categoryIds: List<Long>,
    ) {
        createOfferUseCase.get().execute(
            title = title,
            photoIds = photoIds,
            description = description,
            changeOn = changeOn,
            phone = phone,
            whereToMeet = whereToMeet,
            tags = tags,
            type = type,
            categoryIds = categoryIds,
        )
    }
}