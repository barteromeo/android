package ru.barteromeo.android.ui.createoffer.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.android.material.chip.Chip
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.CreateOfferCategoriesFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.vo.CategoryVo

@AndroidEntryPoint
class CreateOfferCategoriesFragment : BaseFragment() {

    companion object {
        fun newInstance() = CreateOfferCategoriesFragment()
    }

    private val viewModel: CreateOfferCategoriesViewModel by viewModels()
    private lateinit var binding: CreateOfferCategoriesFragmentBinding
    private val selectedCategories = mutableListOf<CategoryVo>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CreateOfferCategoriesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.loadCategories()
        setUpRecyclerView()
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
    }

    private fun setUpRecyclerView() {
        binding.categoriesRecyclerView.layoutManager =
            FlexboxLayoutManager(requireContext(), FlexDirection.ROW, FlexWrap.WRAP).apply {
                justifyContent = JustifyContent.FLEX_START
            }
        val itemAdapter = ItemAdapter<CategoryChipItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)
        binding.categoriesRecyclerView.adapter = fastAdapter
        viewModel.categoryLiveData.observe(viewLifecycleOwner) {
            itemAdapter.add(it.map { vo -> CategoryChipItem(vo) })
        }

        binding.continueButton.setOnClickListener {
            findNavController().navigate(
                R.id.action_navigate_to_create_offer_details,
                arguments.also {
                    it?.putParcelableArrayList("categories", ArrayList(selectedCategories))
                })
        }

        itemAdapter.itemFilter.filterPredicate =
            { item: CategoryChipItem, constraint: CharSequence? ->
                item.category.value.lowercase()
                    .contains(constraint.toString().lowercase(), ignoreCase = true)
            }
        binding.categorySearchEditText.addTextChangedListener {
            itemAdapter.filter(it)
        }
        fastAdapter.onClickListener = { v, _, item, _ ->
            (v as? Chip)?.let {
                it.isSelected = !it.isSelected
                item.isSelected = it.isSelected
            }
            val isSelected = (v as? Chip)?.isSelected ?: false
            if (isSelected) {
                selectedCategories.add(item.category)
            } else {
                selectedCategories.remove(item.category)
            }
            binding.continueButton.isEnabled = selectedCategories.isNotEmpty()
            false
        }
    }

}