package ru.barteromeo.android.ui.start

import dagger.Lazy
import dagger.Reusable
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.usecase.auth.CheckAuthPresentUseCase
import javax.inject.Inject

@Reusable
class StartUseCases @Inject constructor(
    private val checkAuthPresentUseCase: Lazy<CheckAuthPresentUseCase>
) {

    suspend fun checkAuthPresent(): Flow<Boolean> {
        return checkAuthPresentUseCase.get().execute()
    }

}