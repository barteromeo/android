package ru.barteromeo.android.ui.chat.allchats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.AllChatsFragmentBinding
import ru.barteromeo.android.ui.tabs.TabsFragment
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.visible
import javax.inject.Inject

@AndroidEntryPoint
class AllChatsFragment : BaseFragment() {

    companion object {
        fun newInstance() = AllChatsFragment()
    }

    private val viewModel: AllChatsViewModel by viewModels()
    private lateinit var binding: AllChatsFragmentBinding
    private val adapter = FastItemAdapter<ChatItem>()
    private lateinit var parent: TabsFragment

    @Inject
    lateinit var requestManager: RequestManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        parent = parentFragment?.parentFragment as TabsFragment
        binding = AllChatsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.chatsLiveData.observe(viewLifecycleOwner) { it ->
            if (it.isEmpty()) {
                binding.toHomeButton.visible()
                binding.emptyChatsTextView.visible()
                binding.toHomeButton.setOnClickListener {
//                    findNavController().navigate(R.id.action_global_navigation_home)
                    parent.navigateToHome()
                }
            } else {
                adapter.set(it.map { chat ->
                    ChatItem(chat, requestManager) {
                        findNavController().navigate(
                            R.id.navigate_to_single_chat,
                            bundleOf("recipientId" to it.chatUser.id)
                        )
                    }
                })
                binding.chatsRecyclerView.layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                binding.chatsRecyclerView.adapter = adapter
            }
        }
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.getAllChats()
    }

}