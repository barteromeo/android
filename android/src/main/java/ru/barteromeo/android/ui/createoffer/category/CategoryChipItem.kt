package ru.barteromeo.android.ui.createoffer.category

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.databinding.CategoryChipItemBinding
import ru.barteromeo.android.vo.CategoryVo

class CategoryChipItem(val category: CategoryVo) :
    AbstractBindingItem<CategoryChipItemBinding>() {
    override val type: Int
        get() = -1

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): CategoryChipItemBinding {
        return CategoryChipItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: CategoryChipItemBinding, payloads: List<Any>) {
        binding.chipRoot.text = category.value
    }
}