package ru.barteromeo.android.ui.offer

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.databinding.OfferGalleryItemBinding

class OfferGalleryItem(
    private val requestManager: RequestManager,
    private val url: String
) : AbstractBindingItem<OfferGalleryItemBinding>() {


    override fun bindView(binding: OfferGalleryItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        requestManager.load(url).centerCrop().into(binding.offerGalleryImageView)
    }

    override val type: Int
        get() = -1

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): OfferGalleryItemBinding {
        return OfferGalleryItemBinding.inflate(inflater, parent, false)
    }
}