package ru.barteromeo.android.ui.createoffer.type

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.CreateOfferTypeFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.domain.model.OfferType

@AndroidEntryPoint
class CreateOfferTypeFragment : BaseFragment() {

    companion object {
        fun newInstance() = CreateOfferTypeFragment()
    }

    private val viewModel: CreateOfferTypeViewModel by viewModels()
    private lateinit var binding: CreateOfferTypeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CreateOfferTypeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
    }

    private fun setUpAdapter() {
        val itemAdapter = ItemAdapter<CreateOfferTypeItem>()
        val items = OfferType.values().map {
            CreateOfferTypeItem(it, ::navigateToOfferCategories)
        }

        itemAdapter.add(
            items
        )
        val fastAdapter = FastAdapter.with(itemAdapter)

        binding.typesRecyclerView.adapter = fastAdapter
        binding.typesRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
        }
    }

    private fun navigateToOfferCategories(offerType: OfferType) {
        findNavController().navigate(
            R.id.action_navigate_to_create_offer_categories, bundleOf(
                "type" to offerType
            )
        )
    }

}