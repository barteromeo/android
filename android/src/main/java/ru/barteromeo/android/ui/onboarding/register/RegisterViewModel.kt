package ru.barteromeo.android.ui.onboarding.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val registerUseCases: RegisterUseCases
) : BaseViewModel() {

    val loggedInLiveData: LiveData<Boolean>
        get() = loggedInMutableLiveData

    private val loggedInMutableLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun onRegisterClick(
        email: String,
    ) {
        viewModelScope.launch {
            try {
                registerUseCases.sendConfirmRequest(email)
            } catch (e: Exception) {
                mutableErrorLiveData.postValue(e)
            }
        }.invokeOnCompletion {
            if (it != null) {
                mutableErrorLiveData.postValue(it)
            } else {
                loggedInMutableLiveData.postValue(true)
            }
        }

    }

}