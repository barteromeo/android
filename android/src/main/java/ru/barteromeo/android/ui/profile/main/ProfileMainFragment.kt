package ru.barteromeo.android.ui.profile.main

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.RequestManager
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.ProfileMainFragmentBinding
import ru.barteromeo.android.util.AddPhotoBottomSheetFragment
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.MultimediaAttachHelper
import javax.inject.Inject

@AndroidEntryPoint
class ProfileMainFragment : BaseFragment(), AddPhotoBottomSheetFragment.Listener {

    companion object {
        fun newInstance() = ProfileMainFragment()
    }

    override fun onDetach() {
        super.onDetach()
        multimediaAttachHelper.clear()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        registerActivityResultContracts()
    }

    @Inject
    lateinit var multimediaAttachHelper: MultimediaAttachHelper

    @Inject
    lateinit var requestManager: RequestManager

    private val viewModel: ProfileMainViewModel by viewModels()
    private lateinit var binding: ProfileMainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProfileMainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.userLiveData.observe(viewLifecycleOwner) { user ->
            requestManager.load(user.photo).placeholder(R.drawable.ic_twotone_person_24)
                .into(binding.profileAvatarImageView)
            binding.nameProfileMenuItem.show("${user.name} ${user.surname}", "Имя Фамилия")
            binding.nameProfileMenuItem.setOnClickListener { findNavController().navigate(R.id.action_navigate_to_change_name) }
            binding.contactsProfileMenuItem.show("Контакты")
            binding.contactsProfileMenuItem.setOnClickListener { findNavController().navigate(R.id.action_navigate_to_change_phone) }
            binding.myOffersProfileMenuItem.show("Мои предложения")
            binding.myOffersProfileMenuItem.setOnClickListener { findNavController().navigate(R.id.action_navigate_to_my_offers) }
            binding.wishlistProfileMenuItem.show("Избранное")
            binding.wishlistProfileMenuItem.setOnClickListener { findNavController().navigate(R.id.action_navigate_to_wishlist) }
            binding.changeAvatarButton.setOnClickListener {
                onAddPhoto()
            }
        }
        viewModel.fetchUserInfo()

    }

    private fun onAddPhoto() {
        AddPhotoBottomSheetFragment.newInstance()
            .show(childFragmentManager, AddPhotoBottomSheetFragment.TAG)
    }

    private fun registerActivityResultContracts() {
        with(multimediaAttachHelper) {
            registerOpenMultipleDocumentContract(this@ProfileMainFragment) { uris ->
                if (!uris.isNullOrEmpty()) {
                    viewModel.uploadPhoto(uris.first())
                }
            }
            registerTakePhotoContract(this@ProfileMainFragment) { photoUri ->
                if (photoUri != null) {
                    viewModel.uploadPhoto(photoUri)
                }
            }
            registerCameraPhotoPermissionContract(this@ProfileMainFragment) { isAllowed ->
                if (isAllowed) {
                    requestPhotoFromCamera()
                }
            }
        }
    }

    private fun requestPhotoFromCamera() {
        multimediaAttachHelper.launchTakePhotoContract(this)
    }

    override fun onTakeFromCamera() {

        context?.let {
            val isAllowed =
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            if (isAllowed) {
                multimediaAttachHelper.launchTakePhotoContract(this)
            } else {
                multimediaAttachHelper.launchCameraPermissonContract(MultimediaAttachHelper.MultimediaContentType.IMAGE)
            }
        }
    }

    override fun onOpenGallery() {
        multimediaAttachHelper.launchOpenMultipleDocumentContract(MultimediaAttachHelper.Type.IMAGE)
    }


}