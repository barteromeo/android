package ru.barteromeo.android.ui.home

import ru.barteromeo.android.R
import ru.barteromeo.android.ui.offer.CategoryFormatter
import ru.barteromeo.android.util.ResourceUtils
import ru.barteromeo.android.vo.SearchOfferVo
import ru.barteromeo.domain.model.Offer
import ru.barteromeo.domain.model.SearchOffer
import java.util.*
import javax.inject.Inject

@Suppress("StringLiteralDuplication")
class SearchFormatter @Inject constructor(
    private val resourceUtils: ResourceUtils,
    private val categoryFormatter: CategoryFormatter
) {

    fun format(offer: Offer): SearchOfferVo {
        return SearchOfferVo(
            id = offer.id,
            title = offer.title,
            description = offer.description,
            photos = offer.photos,
            views = offer.views,
            offerStatus = offer.offerStatus,
            offerType = offer.offerType,
            categories = offer.categories.map { category -> categoryFormatter.format(category) },
            userPhoto = offer.user.photo,
            userName = "${offer.user.name}\u00a0${offer.user.surname}",
            userPhone = offer.user.phone ?: resourceUtils.getString(R.string.title_no_phone_number),
            updated = offer.updated,
            address = offer.location.address,
            metro = offer.location.metro,
            city = offer.location.city,
            inWishlist = offer.inWishlist,
        )
    }

    fun format(offer: SearchOffer): SearchOfferVo {
        return SearchOfferVo(
            id = offer.id,
            title = offer.title,
            description = offer.description,
            photos = offer.photos,
            views = offer.views,
            offerStatus = offer.offerStatus,
            offerType = offer.offerType,
            categories = offer.categories.map { category -> categoryFormatter.format(category) },
            userPhoto = offer.user.photo,
            userName = "${offer.user.name}\u00a0${offer.user.surname}",
            userPhone = offer.user.phone ?: resourceUtils.getString(R.string.title_no_phone_number),
            updated = Date(),
            address = offer.location.address,
            metro = offer.location.metro,
            city = offer.location.city,
            inWishlist = offer.inWishlist,
        )
    }

}