package ru.barteromeo.android.ui.profile.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.RequestManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.WishlistFragmentBinding
import ru.barteromeo.android.ui.home.OfferLoadingStateAdapter
import ru.barteromeo.android.ui.home.OfferPagingAdapter
import ru.barteromeo.android.ui.tabs.TabsFragment
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.gone
import ru.barteromeo.android.util.visible
import javax.inject.Inject

@AndroidEntryPoint
class WishlistFragment : BaseFragment() {

    private val viewModel: WishlistViewModel by viewModels()
    private var _binding: WishlistFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var searchJob: Job? = null
    private lateinit var adapter: OfferPagingAdapter
    private lateinit var parent: TabsFragment

    @Inject
    lateinit var requestManager: RequestManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        parent = (parentFragment?.parentFragment as TabsFragment)
        _binding = WishlistFragmentBinding.inflate(inflater, container, false)
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.getWishlist()
        setUpAdapter()
        binding.offerSwipeRefreshLayout.setOnRefreshListener {
            viewModel.getWishlist()
        }
        return binding.root
    }

    private fun setUpAdapter() {
        adapter = OfferPagingAdapter(
            ::onOfferClicked,
            ::onFavouriteClicked,
            requestManager = requestManager
        )
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.offersLiveData.observe(viewLifecycleOwner) {
                adapter.submitData(viewLifecycleOwner.lifecycle, it)
            }
        }

        binding.offersRecyclerView.apply {
            layoutManager = GridLayoutManager(requireContext(), 1)
            setHasFixedSize(true)
        }
        binding.offersRecyclerView.adapter = adapter.withLoadStateFooter(
            footer = OfferLoadingStateAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            if (loadState.append.endOfPaginationReached) {
                if (adapter.itemCount < 1) {
                    binding.emptyWishlistTextView.visible()
                    binding.toHomeButton.visible()
                    binding.toHomeButton.setOnClickListener {
                        parent.navigateToHome()
                    }
                }
            }
            if (loadState.mediator?.refresh is LoadState.Loading) {

                if (adapter.snapshot().isEmpty()) {
                    binding.progress.visible()
                }
                binding.errorTxt.gone()

            } else {
                binding.progress.gone()
                binding.offerSwipeRefreshLayout.isRefreshing = false

                val error = when {
                    loadState.mediator?.prepend is LoadState.Error -> loadState.mediator?.prepend as LoadState.Error
                    loadState.mediator?.append is LoadState.Error -> loadState.mediator?.append as LoadState.Error
                    loadState.mediator?.refresh is LoadState.Error -> loadState.mediator?.refresh as LoadState.Error

                    else -> null
                }
                error?.let {
                    if (adapter.snapshot().isEmpty()) {
                        binding.errorTxt.visible()
                        binding.errorTxt.text = it.error.localizedMessage
                    }

                }

            }
        }

    }

    private fun onFavouriteClicked(id: Long, inWishlist:Boolean, position: Int) {
        viewModel.addToWishlist(id, inWishlist)
    }

    private fun onOfferClicked(id: Long) {
        findNavController().navigate(R.id.action_navigate_to_offer, bundleOf("id" to id))
    }


}