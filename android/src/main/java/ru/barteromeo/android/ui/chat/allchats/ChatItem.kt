package ru.barteromeo.android.ui.chat.allchats

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.ChatItemBinding
import ru.barteromeo.android.util.setTextOrGone
import ru.barteromeo.domain.model.Chat

class ChatItem(
    private val chat: Chat,
    private val requestManager: RequestManager,
    private val onItemClicked: (Chat) -> Unit,
) : AbstractBindingItem<ChatItemBinding>() {

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ChatItemBinding {
        return ChatItemBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ChatItemBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        requestManager.load(chat.chatUser.photo).placeholder(R.drawable.ic_twotone_person_24)
            .into(binding.avatarImageView)
        binding.nameTextView.text = "${chat.chatUser.name} ${chat.chatUser.surname}"
        binding.lastMessageTextView.setTextOrGone(chat.lastMessage?.message)
        binding.root.setOnClickListener {
            onItemClicked(chat)
        }
    }

    override val type: Int
        get() = -1
}