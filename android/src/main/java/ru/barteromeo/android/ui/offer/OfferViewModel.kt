package ru.barteromeo.android.ui.offer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.android.vo.OfferVo
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class OfferViewModel @Inject constructor(
    private val offerUseCases: OfferUseCases,
    private val offerFormatter: OfferFormatter
) : BaseViewModel() {

    val searchOfferLiveData: LiveData<OfferVo>
        get() = mutableOfferLiveData

    private val mutableOfferLiveData = MutableLiveData<OfferVo>()

    fun getOffer(offerId: Long) {
        viewModelScope.launch {
            offerUseCases
                .getOfferById(offerId)
                .catch {
                    Timber.e(it)
                }
                .onEach {
                    mutableOfferLiveData.postValue(offerFormatter.format(it))
                }
                .launchIn(this)
        }
    }

}