package ru.barteromeo.android.ui.createoffer.details

import android.net.Uri

sealed class PhotoVo {

    data class Local(
        val uri: Uri,
        var state: State,
        var photoId: String?,
    ) : PhotoVo() {

        enum class State {
            LOADING, LOADED, LOAD_FAILED
        }
    }

    data class Remote(
        val orig: String
    ) : PhotoVo()

}
