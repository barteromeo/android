package ru.barteromeo.android.ui.profile.phone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.barteromeo.android.util.BaseViewModel
import ru.barteromeo.domain.model.User
import javax.inject.Inject

@HiltViewModel
class ChangePhoneViewModel @Inject constructor(
    private val changeNameUseCases: ChangePhoneUseCases
) : BaseViewModel() {

    val userLiveData: LiveData<User>
        get() = userMutableLiveData

    private val userMutableLiveData: MutableLiveData<User> = MutableLiveData()

    fun changePhone(phone: String) {
        viewModelScope.launch(defaultExceptionHandler) {
            changeNameUseCases.changePhone(phone)
        }
    }

    fun fetchUser() {
        viewModelScope.launch {
            changeNameUseCases.fetchUser().onEach {
                userMutableLiveData.postValue(it)
            }.catch {
                mutableErrorLiveData.postValue(it)
            }.launchIn(this)
        }
    }

}