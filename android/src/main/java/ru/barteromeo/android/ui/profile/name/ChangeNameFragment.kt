package ru.barteromeo.android.ui.profile.name

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.databinding.ChangeNameFragmentBinding
import ru.barteromeo.android.util.BaseFragment
import ru.barteromeo.android.util.InputTextRecyclerItem

@AndroidEntryPoint
class ChangeNameFragment : BaseFragment() {

    companion object {
        fun newInstance() = ChangeNameFragment()
    }

    private val viewModel: ChangeNameViewModel by viewModels()
    private lateinit var binding: ChangeNameFragmentBinding
    private val detailAdapter = FastItemAdapter<InputTextRecyclerItem<String>>().apply {
        setHasStableIds(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ChangeNameFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError(it)
        }
        viewModel.userLiveData.observe(viewLifecycleOwner) {
            if (it != null) {
                setUpDetails(it.name, it.surname)
            }
        }
        viewModel.fetchUser()
    }

    private fun setUpDetails(name: String, surname: String) {
        val detailItems = mutableListOf<InputTextRecyclerItem<String>>()
        with(detailItems) {
            add(
                InputTextRecyclerItem("Имя", name) {
                    it.orEmpty()
                }
            )
            add(
                InputTextRecyclerItem("Фамилия", surname) {
                    it.orEmpty()
                }
            )
        }
        detailAdapter.itemAdapter.set(detailItems)
        binding.changeNameRecyclerView.adapter = detailAdapter
        binding.changeNameRecyclerView.layoutManager = LinearLayoutManager(
            requireContext(), LinearLayoutManager.VERTICAL,
            false
        )
        binding.changeNameButton.setOnClickListener {
            viewModel.changeName(
                name = detailItems.first().produce(),
                surname = detailItems[1].produce()
            )
        }
    }

}