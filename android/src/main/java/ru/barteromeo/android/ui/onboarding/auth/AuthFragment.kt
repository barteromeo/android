package ru.barteromeo.android.ui.onboarding.auth

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.barteromeo.android.R
import ru.barteromeo.android.databinding.AuthFragmentBinding

@AndroidEntryPoint
class AuthFragment : Fragment(), LifecycleObserver {

    companion object {
        fun newInstance() = AuthFragment()
    }


    private val viewModel: AuthViewModel by viewModels()
    private lateinit var binding: AuthFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AuthFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @Suppress("Unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreated() {
        requireActivity().lifecycle.removeObserver(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().lifecycle.addObserver(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        binding.logInButton.setOnClickListener {
            findNavController().navigate(R.id.navigation_login_flow)
        }

        binding.registerButton.setOnClickListener {
            findNavController().navigate(R.id.navigation_register_flow)
        }
    }

    override fun onDetach() {
        super.onDetach()
    }

}