package ru.barteromeo.android.ui.onboarding.password.recovery.success

import dagger.hilt.android.lifecycle.HiltViewModel
import ru.barteromeo.android.util.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class PasswordRecoverySuccessViewModel @Inject constructor() : BaseViewModel() {

}