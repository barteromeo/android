package ru.barteromeo.android.ui.profile.phone

import dagger.Lazy
import kotlinx.coroutines.flow.Flow
import ru.barteromeo.domain.model.User
import ru.barteromeo.domain.usecase.user.GetCurrentUserUseCase
import ru.barteromeo.domain.usecase.user.UpdatePhoneUseCase
import javax.inject.Inject

class ChangePhoneUseCases @Inject constructor(
    private val changePhoneUseCase: Lazy<UpdatePhoneUseCase>,
    private val fetchUserUseCase: Lazy<GetCurrentUserUseCase>,
) {

    suspend fun changePhone(phone: String) {
        changePhoneUseCase.get().execute(phone)
    }

    suspend fun fetchUser(): Flow<User?> {
        return fetchUserUseCase.get().execute()
    }

}